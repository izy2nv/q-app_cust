const express = require('express');
const router = express.Router();
const rfq = require('../models/rfq');
const Customer = require('../models/customer');
const Cust_Company = require('../models/cust_company');
const crypto = require('crypto');
const async = require('async');
const config = require('../config/db');
const MongoClient = require('mongodb').MongoClient;
const CustEmailModule = require('../emails/cust_emails');
const ObjectId = require('mongodb').ObjectID;
const Common = require('../models/common');
const RfqEmailModule = require('../emails/rfq_emails');
const Ch = require('../models/ch');

// // Get all rfqs belonging to a particular company
// router.post('/rfqs_all', (req, res) => {
//     Cust_Company.getVerifiedCompanies(req.body, (err, companies) => {
//         if (err) throw err;
//         res.json(companies);
//     });
// });

// Get an rfq by rfqID
router.post('/rfq', (req, res) => {
  async.waterfall([
    function (done) {
      rfq.getRfqByRfqSeq(req.body.rfqSeq, (err, rfq) => {
        if (err) throw err;
        delete rfq._id;
        // console.log('YYYYYYYYYYYY');
        // console.log(rfq);
        // console.log('YYYYYYYYYYYY');
        done(err, rfq);
        // res.json({success: true, rfq: rfq});
      });
    },
    // Get primary contact by Id
    function (rfq, done) {
      Customer.getCustomerById(rfq.pri_contact.oid, (err, pri_contact) => {
        if (err) throw err;
        delete pri_contact._id;
        rfq.pri_contact = pri_contact;
        done(err, rfq);
        // res.json({ success: true, rfq: rfq });
      });
    },
    // Get CH info for each Quote
    function (rfq, done) {
      var chsArr = [];
      // retrieve each ch for each quote
     for (var i = 0; i < rfq.quotes.length; i++) {
      chsArr.push(rfq.quotes[i].ch.oid);
     }
     Common.getMultipleObjsByIdArray(chsArr,'chs', (err, chsArray) => {
       if (err) throw err;
       var quotes = Common.processMultipleObjsArray(chsArray, rfq.quotes, 'ch');
       for (var i = 0; i < quotes.length; i++) {
         delete quotes[i].ch._id;
       }
       rfq.quotes = quotes;
       res.json({ success: true, rfq: rfq });
      //  done(err, rfq);
     });
    }
    // function (rfq, done) {
    //   idArr = [];
    //   for (var i = 0; i < rfq.quotes.length; i++) {
    //     idArr.push(ObjectId(rfq.quotes[i]._id));
    //   }
    //   Ch.getChDetails(idArr, (err, chDetailsArray) => {
    //     if (err) throw err;
    //     var quotes = [];
    //     for (var i = 0; i < chDetailsArray.length; i++) {
    //       for (var j = 0; j < rfq.quotes.length; j++) {
    //         if (rfq.quotes[j]._id == chDetailsArray[i]._id) {
    //           rfq.quotes[j].ch = chDetailsArray[i]
    //         }
    //       }
    //     }

    //     for (var i = 0; i < rfq.quotes.length; i++) {
    //       delete rfq.quotes[i]._id;
    //       delete rfq.quotes[i].ch._id;
    //     }
    //     res.json({ success: true, rfq: rfq });
    //   });
    // }
  ]);
});

// Create new RFQ
router.post('/new_rfq', (req, res, next) => {
  console.log('req body');
  console.log(req.body);
  console.log('req body');
  const custId = req.body.pri_contact;

  var pri_contact = {
    "$ref": "customers",
    "$id": req.body.pri_contact
  };
  var alt_contacts = [];
  req.body.pri_contact = pri_contact;
  req.body.status = 1; // status = 1 (rfq is open), = 2 (closed)
  req.body.state = 1; // state = 1 (rfq is unarchived), = 2 (archived), = 3 (deleted)
  req.body.quotes = [];

  async.waterfall([
    function (done) {
      if (req.body.alt_contacts === null) {
        req.body.alt_contacts = alt_contacts;
        done(null, req.body);
      } else {
        Customer.getMultipleCustomesByEmailArray(req.body.alt_contacts, (err, custArr) => {
          // NOTE: WAE HAVE TO CHECK HERE THAT THE CONTACTS TO BE ADDED ARE VERIFIED
          if (err) throw err;
          for (var i = 0; i < custArr.length; i++) {
            delete custArr[i]._id;
          }
          req.body.alt_contacts = custArr;
          done(err, req.body);
        });
      }
    },
    function (rfqObj, done) {
      Common.getNextSequenceValue('rfqSeq', (err, rfqSeqObj) => {
        if (err) throw err;
        rfqObj.rfqSeq = rfqSeqObj.sequence_value;
        async.waterfall([
          function (done) {
            Customer.getCustomerById(ObjectId(custId), (err, cust) => {
              if (err) throw err;
              console.log('myyyyyyyy');
              console.log(cust.min.oid);
              console.log('myyyyyyyy');
              // custType = cust.custType;
              if (cust.custType === 1) {
                rfqObj.orig_ministry = cust.min.oid;
                rfqObj.orig_dept = cust.dept;
              } else if (cust.custType === 2) {

              }
              done(err, rfqObj);
            });
          },
          function (rfqObj, done) {
            rfq.saveNewRfq(rfqObj, (err, savedRfqObj) => {
              if (err) throw err;
              res.json({ sucess: true, message: 'SUCCESS: RFQ created' });
              done(err, savedRfqObj);
            });
          },
          // Send Emails to customers and CHs on the rfq
          function (savedRfqObj, done) {
            // Get chs whose prod cat matches the rfq's cat
            const param = { "prodsCat": savedRfqObj.cat };
            Ch.getAllChsByParam(param, (err, chs) => {
              var designatedChArr = [];
              if (err) throw err;
              for (var i = 0; i < chs.length; i++) {
                for (var j = 0; j < chs[i].contacts.length; j++) {
                  if (chs[i].contacts[j].rank === 1) {
                    designatedChArr.push(chs[i].contacts[j]);
                  }
                }
              }
              done(err, designatedChArr, savedRfqObj);
            });
          },
          // Send email to each designated ch receiving rfq requests
          function (designatedChArr, savedRfqObj, done) {
            // RfqEmailModule.sendNewRfqEmailToCH(designatedChArr, savedRfqObj, req.headers.host, (err) => {
            //   if (err) throw err;
            // });
          }
        ]);
      });
    }
  ]);

});

// Get all rfqs for a logged-in user 
router.post('/cust_rfqs', (req, res) => {
  async.waterfall([
    function (done) {
      var query = null;
      if (req.body.id) {
        // Indicates a non-govt user
        query = { "pri_contact": { "$ref": "customers", "$id": req.body.id } };
      } else if (req.body.min) {
        query = { 'orig_ministry':  ObjectId(req.body.min), 'orig_dept': req.body.dept };
      }
      rfq.getAllRfqsForLoggedInUser(query, (err, rfqObjsArray) => {
        if (err) throw err;
        // res.json({success: true, rfqsArray: rfqObjsArray});
        done(err, rfqObjsArray);
      });
    },
    function(rfqObjsArray, done) {
      var contactsArr = [];
      // retrieve each pri_contact for each rfq
     for (var i = 0; i < rfqObjsArray.length; i++) {
       contactsArr.push(ObjectId(rfqObjsArray[i].pri_contact.oid));
     }
     Customer.getMultipleCustomersByIdArray(contactsArr, (err, custArr) => {
       if (err) throw err;
        var rfqs = rfq.processPriContInRfqsArray(custArr, rfqObjsArray);
    done(err, rfqs);
     });
    },
    // IMPORTANT. TO BE WORKED ON FOR INDIVUAL USERS
    // function (rfqObjsArray, done) {
    //   // rfqObjsArray[0] is used here cos its d same pri_contact for all the rfqs in the rfqObjsArray
    //   if (rfqObjsArray.length > 0) {
    //     var dbRef = rfqObjsArray[0].pri_contact;
    //     // We are getting the pri_contact incase a higher-level staff or owner is viewing the rfq
    //     rfq.getPriContactDetails(dbRef, (err, pri_cont) => {
    //       if (err) throw err;
    //       done(err, rfqObjsArray, pri_cont);
    //     });
    //   } else {
    //     res.json({ success: true, rfqsArray: rfqObjsArray });
    //   }
    // },
    function (rfqs, done) {
      res.json({ sucess: true, rfqsArray: rfqs });
    }
  ]);
});

// Delete an rfq by rfqSeq
router.post('/deleteOne', (req, res) => {
  console.log(req.body);
  async.waterfall([
    // Get details of rfq, so it can be included in the email to CH
    function (done) {
      rfq.getRfqByRfqSeq(req.body.rfqSeq, (err, rfqObj) => {
        if (err) throw err;
        done(err, rfqObj);
      });
    },
    // Delete rfq from db
    function (rfqObj, done) {
      rfq.deleteRfqByRfqSeq(req.body.rfqSeq, (err, status) => {
        if (err) throw err;
        res.json({ success: true, message: 'RFQ successfully deleted' });
        done(err, rfqObj);
      });
    },
    // Save the deleted rfq to 'canceled_rfqs' collection in db
    function (rfqObj, done) {
      delete rfqObj._id;
      rfqObj.cancel_reason = req.body.reason;
      rfqObj.cancel_initiator = {
        "$ref": "customers",
        "$id": req.body.custId
      };
      rfq.saveDeletedRfq(rfqObj, (err, rfq) => {
        if (err) throw err;
        done(err, rfqObj);
      });
    },
    // Send cancellation email to chs on that rfq
    function (rfqObj, done) {
      console.log('sending email to chs');
      // RfqEmailModule.sendRfqCancellationEmailToCH()
    }
  ]);

});

// // Create new RFQ
// router.post('/new_rfq', (req, res, next) => {
//     console.log('testing');
//     var pri_contact =  {
//         "$ref": "customers",
//         "$id": req.body.pri_contact
//     };
//     var alt_contacts = []; 
//     if (req.body.alt_contacts === null) {
//         req.body.alt_contacts = alt_contacts;
//     }
//     req.body.pri_contact = pri_contact;
//     req.body.status = 1;
//     Common.getNextSequenceValue('rfqSeq', (err, rfqSeqObj) => {
//         if (err) throw err;
//         req.body.rfqSeq = rfqSeqObj.sequence_value;
//         async.waterfall([
//             function(done) {
//                 rfq.saveNewRfq(req.body, (err, savedRfqObj) => {
//                     if (err) throw err;
//                     res.json({sucess: true, message: 'SUCCESS: RFQ created'});
//                     done(err, savedRfqObj);
//                 });
//             },
//             // Send Emails to customers and CHs on the rfq
//             function(savedRfqObj, done) {
//                 console.log(savedRfqObj);
//             }
//         ]);
//     });
// });

// // Get all rfqs for a customer based on the cust_ID. 
// router.post('/cust_rfqs', (req, res) => {
//     async.waterfall([
//         function(done) {
//             const query = {"pri_contact": {"$ref": "customers", "$id": req.body.id}};
//             rfq.getAllRfqsByPrimaryContact(query, (err, rfqObjsArray) => {
//                 if (err) {
//                     res.json(err);
//                 }
//                     done(err, rfqObjsArray);
//             });
//         },
//         function (rfqObjsArray, done) {
//             // rfqObjsArray[0] is used here cos its d same pri_contact for all the rfqs in the rfqObjsArray
//             // console.log('array lenght is '+ rfqObjsArray.length);
//             if (rfqObjsArray.length > 0) {
//                 var dbRef = rfqObjsArray[0].pri_contact;
//                 // We are getting the pri_contact incase a higher-level staff or owner is viewing the rfq
//            rfq.getPriContactDetails(dbRef, (err, pri_cont) => {
//                if (err) throw err;
//                done(err, rfqObjsArray, pri_cont);
//            });
//             } else {
//                 res.json({success: true, rfqsArray: rfqObjsArray});
//             }
//         },


//          // Get all alternate contacts  
//          function(rfqObjsArray, pri_cont, done) {
//             rfq.getAltcontactsDetails(rfqObjsArray, 'customers', (err, allContacts) => {

//             });

//         },

//         // // Get all alternate contacts  
//         // function(rfqObjsArray, pri_cont, done) {
//         //     async.forEachOf(rfqObjsArray, function (value, key, callback) {
//         //         console.log('********** ALTERNATE CONTACTS ********');
//         //         console.log(rfqObjsArray[key].alt_contacts);
//         //         console.log('********** ALTERNATE CONTACTS ********');


//         //         if (rfqObjsArray[key].alt_contacts.length > 0) {
//         //             rfq.getAltcontactsDetails(rfqObjsArray[key].alt_contacts, rfqObjsArray[key].alt_contacts[0].namespace, (err, allContacts) => {
//         //                 if (err) throw err;
//         //                 async.forEachOf(allContacts, (value, key, callback) => {
//         //                     // delete allContacts[key]['un'];
//         //                     delete allContacts[key].un;
//         //                 });
//         //                 rfqObjsArray[key].alt_contacts = allContacts;
//         //                 done(err, rfqObjsArray, pri_cont);
//         //             });
//         //         } else {
//         //             res.json({sucess: true, rfqsArray: rfqObjsArray});
//         //         }
//         //     });

//         // },
//         function(rfqObjsArray, pri_cont, done) {
//             console.log('*******rfqObjsArray******');
//             console.log(rfqObjsArray); 
//             console.log('******rfqObjsArray*******');

//             // console.log('******PRIMARY CONTACT DETAILS******');
//             // console.log(pri_cont);
//             // console.log('******PRIMARY CONTACT DETAILS******');

//             for (var i = 0; i < rfqObjsArray.length; i++) {
//                 rfqObjsArray[i].pri_contact = pri_cont;
//             }
//             res.json({sucess: true, rfqsArray: rfqObjsArray});
//             // const rfqDetails = {

//             // };
//         }
//     ]);
// });


// This applies to adding alt contacts to an EXISTING RFQ
router.post('/addContact', (req, res, next) => {
  // Checks to ensure a primary contact can't be added as an alternate contact
  if (req.body.email == req.body.pri_contact) {
    res.json({ success: false, message: 'Failed Operation. You are the primary contact on this RFQ.' });
    return;
  } else {
    async.waterfall([
      function (done) {
        // check first if the contact is verified
        Customer.getCustomerByEmail(req.body.email, (err, cust) => {
          if (err) {
            res.json({ error: err });
          }
          if (!cust) {
            res.json({ success: false, message: 'No such customer in our records. Please verify email entered.' });
            return;
          }
          if (!cust.verified) {
            res.json({ success: false, message: 'This contact cannot be added as they have not yet been verified.' });
            return;
          } else {
            done(err, cust);
          }
        });
      },
      function (cust, done) {
        // Prevent duplicating contact in the alt_contacts array of the RFQ
        rfq.getRfqByRfqSeq(req.body.rfqSeq, (err, rfqObj) => {
          if (err) throw err;
          delete cust._id;
          var isCont = rfqObj.alt_contacts.filter(function (cont) {
            return cont.email === req.body.email;
          });
          if (isCont.length > 0) {
            res.json({ success: false, message: 'Contact already exists on this RFQ.' });
          } else {
            done(err, rfqObj, cust);
          }
        });
      },
      function (rfqObj, cust, done) {
        // Add the cust to the alt_contacts array of the rfq
        rfqObj.alt_contacts.push(cust);
        rfq.updateRfq({ '_id': ObjectId(rfqObj._id) }, { $set: { "alt_contacts": rfqObj.alt_contacts } }, (err) => {
          if (err) {
            res.json({ success: false, message: 'Server Error: Please try again later.' });
          }
          res.json({ success: true, message: 'Contact successfully added!', cust: cust });

          // send email to alt contact added
          RfqEmailModule.sendAddDelAltContEmail(req.body.rfqSeq, cust, true, (err) => {
            if (err) throw err;
          });
        });
      }
    ]);
  }
});

// Delete alternate contact from an rfq
router.post('/delete_altcontact', (req, res, next) => {
  async.waterfall([
    function (done) {
      // Get rfq by rfqSeq
      rfq.getRfqByRfqSeq(req.body.rfqSeq, (err, retRfq) => {
        if (err) {
          res.json({ success: false, message: 'Server Error. Please try again later.' });
          return;
        }
        done(err, retRfq);
      });
    },
    function (retRfq, done) {
      // get info abt contact being deleted frm db
      Customer.getCustomerByEmail(req.body.email, (err, cust) => {
        if (err) throw err;
        done(err, retRfq, cust);
      });
    },
    function (retRfq, cust, done) {
      // Remove the alt contact having the passed email
      for (var i = 0; i < retRfq.alt_contacts.length; i++) {
        if (retRfq.alt_contacts[i].email == req.body.email) {
          retRfq.alt_contacts.splice(i, 1);
          break;
        }
      }
      // Update the rfq with the resulting alt_contacts array
      rfq.updateRfq({ 'rfqSeq': req.body.rfqSeq }, { $set: { "alt_contacts": retRfq.alt_contacts } }, (err) => {
        if (err) {
          res.json({ success: false, message: 'Server Error. Please try again later.' });
          return;
        }
        res.json({ success: true, message: 'Contact deleted successfully!' });

        // send email to alt contact deleted
        RfqEmailModule.sendAddDelAltContEmail(req.body.rfqSeq, cust, false, (err) => {
          if (err) throw err;
        });
      });
    }
  ]);
});

// Update an rfq
router.post('/update_rfq', (req, res, next) => {
  async.waterfall([
    // update the rfq
    function (done) {
      let updateObj = null;
      console.log('GGGGGGGGGG');
      console.log(req.body);
      console.log('GGGGGGGGGG');
      if (req.body.close_reason) {
        console.log('it is closing');
        updateObj = {
          status: 2,
          close_reason: req.body.close_reason
        };
      } else {
        updateObj = req.body;
        console.log('testing archive');
        console.log(updateObj);
        console.log('testing archive');
      }
      rfq.updateRfq({ 'rfqSeq': req.body.rfqSeq }, { $set: updateObj }, (err) => {
        if (err) {
          res.json({ success: false, message: 'Server Error. Please try again later.' });
          return;
        }
        res.json({ success: true, message: 'Rfq updated successfully!' });
        if (!req.body.isArchived) {
          done(err, req.body.rfqSeq);
        }
      });
    },
    function (updatedRfqSeq, done) {
      console.log('TEST');
      // Send Email to CH's on the rfq of this update
    }
  ]);
});

// // Update an rfq
// router.post('/update_rfq', (req, res, next) => {
//   async.waterfall([
//     // update the rfq
//     function (done) {
//       rfq.updateRfq({ 'rfqSeq': req.body.rfqSeq }, { $set: req.body }, (err) => {
//         if (err) {
//           res.json({ success: false, message: 'Server Error. Please try again later.' });
//           return;
//         }
//         res.json({ success: true, message: 'Rfq updated successfully!' });
//         if (!req.body.isArchived) {
//           done(err, req.body.rfqSeq);
//         }
//       });
//     },
//     function (updatedRfqSeq, done) {
//       console.log('TEST');
//       // Send Email to CH's on the rfq of this update
//     }
//   ]);
// });

// Fetch all prods/services categories, to create a new Rfq
router.get('/cats', (req, res, next) => {
  rfq.getAllProdServCats((err, cats) => {
    if (err) throw err;
    delete cats._id;
    delete cats[0]._id;
    res.json({ success: true, categories: cats })
  });
});

module.exports = router;