const express = require('express');
const router = express.Router();
const Ministry = require('../models/ministry');

// Get all ministries
router.post('/ministries', (req, res) => {
  var param = { 'country': req.body.country, 'state': req.body.state };
  Ministry.getMinistries(param, (err, ministries) => {
    if (err) throw err;
    res.json({ success: true, ministries: ministries });
  });
});

module.exports = router;