const express = require('express');
const router = express.Router(); 
// const config = require('../config/db');
// const MongoClient = require('mongodb').MongoClient;
const Company = require('../models/company');
const Common = require('../models/common');
const mongojs = require('mongojs');
var db = mongojs('mongodb://izy:izy@ds145952.mlab.com:45952/mydb_izy');
var formidable = require('formidable');
var fs = require('fs');
path = require('path');
// const uploadDir = path.join(__dirname, '/..', '/..', '/..', '/projects/uploads/');
const uploadDir = './uploads/';
var multer = require('multer');


//i made this  before the function because i use it multiple times for deleting later
// const uploadDir = path.join(__dirname, '/..', '/..', '/..', '/projects/uploads/');

//Get footer links
router.get('/footerlinks', (req, res) => {
    db.footerLinks.find((err, links) => {
        if (err) res.send(err);
        // console.log(links);
        res.json(links);
    });
});

// Get all contract holder companies
router.get('/ch_companies', (req, res) => {
    db.chs.find((err, chs) => {
        if (err) res.send(err);
        console.log(chs);
        res.json(chs);
    });
});

// Get a single ch
router.post('/ch', (req, res) => {
    db.chs.findOne({_id: mongojs.ObjectId(req.body.id)},(err, ch) => {
        console.log(req.body.id);
        if (err) res.send(err);
        console.log(ch);
        res.json(ch);
    });
});



var store = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, uploadDir);
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '.' + file.originalname);
    }
  });
   
  var upload = multer({ storage: store }).single('file');

router.post('/rfq_file_upload', (req, res) => {
    console.log(req.body);
    upload(req, res, (err) => {
        if (err) {
            console.log('err occ');
            return res.status(501).json({error: err});
        }
        console.log({originalname: req.file.originalname, uploadName: req.file.filename})
        return res.json({originalname: req.file.originalname, uploadName: req.file.filename});
    });
});

router.post('/download', (req, res, next) => {
    console.log(req.body['fileName']);
    filepath = path.join(__dirname, '../uploads') + '/' + req.body.fileName;
    // res.json(req.body);
    res.sendFile(filepath);
});

// Get list of countries
router.post('/states', (req, res, next) => {
    Common.getStatesByCountry(req.body, (err, data) => {
        res.json({success: true, states: data.states});
    });
});

// router.post('/new_rfq_file_upload', (req, res) => {
//     var form = new formidable.IncomingForm();
//     form.multiples = true;
//     form.keepExtensions = true;
//     form.uploadDir = uploadDir;
//     form.parse(req, (err, fields, files) => {
//       if (err) return res.status(500).json({ error: err });
//       res.status(200).json({ uploaded: true });
//     });
//     form.on('fileBegin', function (name, file) {
//       const [fileName, fileExt] = file.name.split('.')
//       file.path = path.join(uploadDir, `${fileName}_${new Date().getTime()}.${fileExt}`)
//     });
// });


// //Get single product
// router.get('/prod/:id', (req, res) => {
//     db = mongojs(apiConn, prods);
//     db.products.findOne({_id: mongojs.ObjectId(req.params.id)}, (err, prod) => {
//         if (err) res.send(err);
//         res.json(prod);
//     });
// });

// //Save a product
// router.post('/saveItem', (req, res) => {
//     let item = req.body;
//     if (!item.cost || !item.name || !item.category) {
//         res.status(400);
//         res.json(
//             {'Error': 'Invalid Data'}
//         );
//     } else {
//         let cat = item.category;
//         db.cat.save(item, (err, item) => {
//             if (err) res.send(err);
//             res.json(item);
//         })
//     }
// });

// //Delete a product
// router.delete('/prod/:id', (req, res) => {
//     db = mongojs(apiConn, prods);
//     db.products.remove({_id: mongojs.ObjectId(req.params.id)}, (err, prod) => {
//         if (err) res.send(err);
//         res.json(prod);
//     });
// });

// //Update a product
// router.put('/prod/:id', (req, res) => {
//     let prod = req.body,
//     updateProd = {};
//     if (prod.name) {
//         updateProd.name = prod.name;
//     }
//     if (prod.cost) {
//         updateProd.cost = prod.cost;
//     }
//     if (prod.year) {
//         updateProd.year = prod.year;
//     }
//     if (!updateProd) {
//         res.send(400);
//         res.json(
//             {'Error': 'Bad data'}
//         )
//     } else {
//         db = mongojs(apiConn, prods);
//         db.products.update({_id: mongojs.ObjectId(req.params.id), updateProd}, {}, (err, prod) => {
//             if (err) res.send(err);
//             res.json(prod);
//         });
//     }
// });

module.exports = router;