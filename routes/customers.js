const express = require('express');
const router = express.Router();
const passport = require('passport');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Customer = require('../models/customer');
const Ministry = require('../models/ministry');
const Cust_Company = require('../models/cust_company');
const crypto = require('crypto');
const async = require('async');
const config = require('../config/db');
const MongoClient = require('mongodb').MongoClient;
const CustEmailModule = require('../emails/cust_emails');
const ObjectId = require('mongodb').ObjectID;
const Common = require('../models/common');
const Cust_Auth = require('../models/cust_auth');
const Rfq = require('../models/rfq');
var _ = require('lodash');

// Mailgun config for sending emails
var api_key = 'key-05ff46fdbc55a20993b2a29687412e8b';
var domain = 'sandboxe7580ba510e143d2ada6cfe84a0570c2.mailgun.org';
var mailgun = require('mailgun-js')({ apiKey: api_key, domain: domain });

// Get all verified customer companies
router.post('/cust_companies', (req, res) => {
    Cust_Company.getVerifiedCompanies(req.body, (err, companies) => {
        if (err) throw err;
        res.json(companies);
    });
});

// // Get all verified customer companies
// router.post('/cust_companies', (req, res) => {
//   async.waterfall([
//     function(done) {
//       Cust_Company.getVerifiedCompanies(req.body, (err, companies) => {
//         if (err) throw err;
//         done(err, companies);
//         // res.json(companies);
//     });
//     },
//     function(companies, done) {
//       console.log('COMPANIES');
//       console.log(companies);
//       console.log('COMPANIES');
//       var err;

//       // var dbRef = companies[0].pri_contact;
//       async.forEachOf(companies, function (value, key, callback) {
//         if (companies[key].contacts.length > 0) {
//           Common.getObjectsDetails(companies[key].contacts, companies[key].contacts[0].namespace, (err, allContactObjsDetails) => {
//             if (err) {
//               throw err;
//               err = err;
//               done(err);
//             }
//             console.log(allContactObjsDetails);
//             companies[key].contacts = allContactObjsDetails;
//             // done(err,companies);
//           });
//         }
//       });
//       done(err,companies);
//     },
//     function(companies, done) {
//       res.json(companies);
//     }
//   ]);
// });


// Confirm or deny customer in verification
router.post('/verify_cust', (req, res, next) => {
    const custId = req.body.id.substring(0, req.body.id.length - 1);
    const statusCode = req.body.id.charAt(req.body.id.length - 1);
    async.waterfall([
        function (done) {
            // get customer's details
            Customer.getCustomerById(custId, (err, customer) => {
                if (err) throw err;
                if (!customer) {
                    console.log('NO CUSTOMER');
                    res.json({ success: false, message: 'It seems you previously un-identified this contact. They will have to create a new account to proceed' });
                    return;
                }
                done(err, customer);
            });
        },
        // Set customer's verified field to true or false
        function (customer, done) {
            if (statusCode === 't') {
                Customer.updateCustomer({ '_id': ObjectId(custId) }, { $set: { "verified": true } }, (err) => {
                    if (err) throw err;
                    done(err, customer);
                });
            } else if (statusCode === 'f') {
                Customer.updateCustomer({ '_id': ObjectId(custId) }, { $set: { "verified": false } }, (err) => {
                    if (err) throw err;
                    done(err, customer);
                });
            } else {
                res.json({ success: false, message: 'An Error occured.' });
                return;
            }
        },
        // add to or remove customer from company's contacts list
        function (customer, done) {
            if (customer.custType === 2) {
                const dbRef = customer.comp;
                Common.getEstablishmentDetails(dbRef, (err, company) => {
                    if (err) throw err;
                    // Add customer to company's contacts list
                    if (statusCode === 't') {
                        if (Common.checkForObjInArray(custId, company.contacts) === false) {
                            const new_contact = {
                                $ref: "customers",
                                $id: custId
                            };
                            company.contacts.push(new_contact);
                            Cust_Company.updateCompany({ 'compName': customer.compName }, { $set: { "contacts": company.contacts } }, (err) => {
                                if (err) throw err;
                                res.json({
                                    success: true, message: 'You have successfully confirmed ' + customer.lName + ' ' + customer.mName + ' ' + customer.fName +
                                        ' as a representative of the interests of ' + customer.compName + ' in the use of the Quotes application !'
                                });
                                CustEmailModule.sendCustVerificationEmail(customer, 't', req.headers.host, (err) => {
                                    if (err) throw err;
                                });
                            });
                        } else {
                            res.json({
                                success: true, message: 'You have successfully confirmed ' + customer.lName + ' ' + customer.mName + ' ' + customer.fName +
                                    ' as a representative of the interests of ' + customer.compName + ' in the use of the Quotes application !'
                            });
                            return;
                        }
                    } else if (statusCode === 'f') {
                        // Handles cases where cust was previously verified, and now company unverifies them. This will remove them frm company's contact list
                        if (Common.checkForObjInArray(custId, company.contacts) === true) {
                            console.log('Customer exists as a contact');
                            const custToRemove = {
                                namespace: 'customers',
                                oid: custId
                            };
                            company.contacts.splice(company.contacts.indexOf(custToRemove), 1);
                            Cust_Company.updateCompany({ 'compName': customer.compName }, { $set: { "contacts": company.contacts } }, (err) => {
                                if (err) throw error;
                                res.json({ success: true, message: 'You have successfully un-verified ' + customer.lName + ' ' + customer.mName + ' ' + customer.fName });
                                CustEmailModule.sendCustVerificationEmail(customer, 'f', req.headers.host, (err) => {
                                    if (err) throw err;
                                });
                            });
                        } else {
                            return;
                        }
                    }
                });
                // Cust_Company.getCompanyByName(customer.compName, (err, company) => {
                //     if (err) throw err;
                //     // Add customer to company's contacts list
                //     if (statusCode === 't') {
                //         if (Common.checkForObjInArray(custId, company.contacts) === false) {
                //             const new_contact = {
                //                 $ref: "customers",
                //                 $id: custId
                //             };
                //             company.contacts.push(new_contact);
                //             Cust_Company.updateCompany({ 'compName': customer.compName }, { $set: { "contacts": company.contacts } }, (err) => {
                //                 if (err) throw err;
                //                 res.json({
                //                     success: true, message: 'You have successfully confirmed ' + customer.lName + ' ' + customer.mName + ' ' + customer.fName +
                //                         ' as a representative of the interests of ' + customer.compName + ' in the use of the Quotes application !'
                //                 });
                //                 CustEmailModule.sendCustVerificationEmail(customer, 't', req.headers.host, (err) => {
                //                     if (err) throw err;
                //                 });
                //             });
                //         } else {
                //             res.json({
                //                 success: true, message: 'You have successfully confirmed ' + customer.lName + ' ' + customer.mName + ' ' + customer.fName +
                //                     ' as a representative of the interests of ' + customer.compName + ' in the use of the Quotes application !'
                //             });
                //             return;
                //         }
                //     } else if (statusCode === 'f') {
                //         // Handles cases where cust was previously verified, and now company unverifies them. This will remove them frm company's contact list
                //         if (Common.checkForObjInArray(custId, company.contacts) === true) {
                //             console.log('Customer exists as a contact');
                //             const custToRemove = {
                //                 namespace: 'customers',
                //                 oid: custId
                //             };
                //             company.contacts.splice(company.contacts.indexOf(custToRemove), 1);
                //             Cust_Company.updateCompany({ 'compName': customer.compName }, { $set: { "contacts": company.contacts } }, (err) => {
                //                 if (err) throw error;
                //                 res.json({ success: true, message: 'You have successfully un-verified ' + customer.lName + ' ' + customer.mName + ' ' + customer.fName });
                //                 CustEmailModule.sendCustVerificationEmail(customer, 'f', req.headers.host, (err) => {
                //                     if (err) throw err;
                //                 });
                //             });
                //         } else {
                //             return;
                //         }
                //     }
                // });
            } else if (customer.custType === 1) {
                console.log(customer);
                const dbRef = customer.min;
                Common.getEstablishmentDetails(dbRef, (err, ministry) => {
                    if (err) throw err;
                    console.log(ministry);
                    if (statusCode === 't') {
                        if (Common.checkForObjInArray(custId, ministry.contacts) === false) {
                            const new_contact = {
                                $ref: "customers",
                                $id: custId
                            };
                            ministry.contacts.push(new_contact);
                            Ministry.updateMinistry({ '_id': ObjectId(ministry._id )}, { $set: { "contacts": ministry.contacts } }, (err) => {
                                if (err) throw err;
                                console.log('MINISTRY UPDATED SUCCESSFULLY');
                                res.json({
                                    success: true, message: customer.lName + ' ' + customer.mName + ' ' + customer.fName +' successfully verified!'
                                });
                                CustEmailModule.sendCustVerificationEmail(customer, 't', req.headers.host, (err) => {
                                    if (err) throw err;
                                });
                            });
                        } else {
                            res.json({
                                success: true, message: 'You have successfully confirmed ' + customer.lName + ' ' + customer.mName + ' ' + customer.fName +
                                    ' as a representative of the interests of ' + customer.compName + ' in the use of the Quotes application !'
                            });
                            return;
                        }
                    }
                });

                // Ministry.getMinistryDetails(dbRef, (err, ministry) => {
                //     if (err) throw err;
                //     console.log(ministry);
                //     if (statusCode === 't') {
                //         if (Common.checkForObjInArray(custId, ministry.contacts) === false) {
                //             const new_contact = {
                //                 $ref: "customers",
                //                 $id: custId
                //             };
                //             ministry.contacts.push(new_contact);
                //             Ministry.updateMinistry({ '_id': ObjectId(ministry._id )}, { $set: { "contacts": ministry.contacts } }, (err) => {
                //                 if (err) throw err;
                //                 console.log('MINISTRY UPDATED SUCCESSFULLY');
                //                 res.json({
                //                     success: true, message: customer.lName + ' ' + customer.mName + ' ' + customer.fName +' successfully verified!'
                //                 });
                //                 CustEmailModule.sendCustVerificationEmail(customer, 't', req.headers.host, (err) => {
                //                     if (err) throw err;
                //                 });
                //             });
                //         } else {
                //             res.json({
                //                 success: true, message: 'You have successfully confirmed ' + customer.lName + ' ' + customer.mName + ' ' + customer.fName +
                //                     ' as a representative of the interests of ' + customer.compName + ' in the use of the Quotes application !'
                //             });
                //             return;
                //         }
                //     }
                // });
            }
        }
    ]);
});

// Save un-verified customer's company
router.post('/saveUnverifiedCustComp', (req, res, next) => {
    console.log(req.body);
    Cust_Company.addUnverifiedCompany(req.body, (err, company) => {
        if (err) throw err;
        console.log('Successfully saved unverified company');
        res.json('Successfully saved unverified company');
    });
});

//Create a customer account
router.post('/register', (req, res, next) => {
    var addrObj = {
        addr1: req.body.addr1,
        addr2: req.body.addr2,
        state: req.body.state,
        country: req.body.country
    };

    var unverified_comp = {
        compName: req.body.compName,
        addr: addrObj,
        verified: false
    };

    var newCust = {
        fName: req.body.fName,
        lName: req.body.lName,
        mName: req.body.mName,
        addr: addrObj,
        mobile: null,
        work: req.body.work,
        ext: req.body.ext,
        email: req.body.email,
        verified: false
    };
    if (!req.body.min && !req.body.compName) {
        // Individual user
        newCust.custType = 3;
    }
    // check if a customer having same email as req.body.email, already exists in the customers collection
    Customer.getCustomerByEmail(req.body.email, (err, cust) => {
        if (err) throw err;
        if (cust) {
            console.log('Email exists');
            res.json({ success: false, message: '"' + req.body.email + '"' + ' already exists. Please sign up with a different email address.' });
            return;
        } else if (!cust) {
            console.log('No customer with such email exists');
            async.waterfall([
                // Add new customer to customers collection
                function (done) {
                    var param = null;
                    var collection = null;
                    if (req.body.min) {
                        param = { 'min': req.body.min, 'state': req.body.state, 'country': req.body.country };
                        collection = 'ministries';
                    } else if (req.body.compName) {
                        param = { 'compName': req.body.compName, 'state': req.body.state, 'country': req.body.country };
                        collection = 'cust_companies';
                    }
                    Common.getEstablishmentByParam(param, collection, (err, establishment) => {
                        if (err) throw err;
                        if (req.body.min) {
                            newCust.min = {
                                "$ref": "ministries",
                                "$id": establishment._id
                            };
                            newCust.custType = 1;
                            newCust.dept = req.body.dept; 
                        } else if (req.body.compName) {
                            newCust.comp = {
                                "$ref": "cust_companies",
                                "$id": establishment._id
                            };
                            newCust.custType = 2;
                            newCust.compName = req.body.compName;
                        }
                        done(err, newCust, establishment);
                    });
                },
                // establishment here is either company or ministry
                function(newCust, establishment, done) {
                    // add customer to DB
                    Customer.addCustomer(newCust, (err, cust) => {
                        if (err) {
                            res.json({ success: false, message: err });
                        } else {
                            done(err, cust, establishment);
                        }
                    });
                },
                // Add new customer acct auth info to cust_auth collection
                function (cust, establishment, done) {
                    const authObj = {
                        un: req.body.user,
                        pw: req.body.pw,
                        email: req.body.email,
                        passwordResetToken: null,
                        passwordExpirationDate: null,
                        cust: {
                            $ref: "customers",
                            $id: cust._id
                        }
                    };
                    Cust_Auth.addCustAuth(authObj, (err, authDetails) => {
                        if (err) throw err;
                        res.json({ success: true, message: 'Account registered successfully' });
                    });
                    done(null, establishment);
                },
                function (establishment, done) {
                    CustEmailModule.sendVerifEmailToCustComp(establishment, newCust, req.headers.host, (err) => {
                        if (err) throw err;
                        console.log('Verification Email has been sent!');
                    });
                }
            ]);
        }
    });
});

// Authenticate customer on log-in attempt from the client
router.post('/authenticate', (req, res, next) => {
    const username = req.body.user;
    const password = req.body.pw;
    async.waterfall([
        function (done) {
            Customer.getAcctObjByUsername(username, (err, acctObj) => {
                if (err) throw err;
                if (!acctObj) {
                    console.log('NO USER TO AUTHENTICATE');
                    return res.json({ success: false, message: 'User not found. Please verify your user ID.' });
                }
                done(err, acctObj);
            });
        },
        function (acctObj, done) {
            Customer.comparePasswords(password, acctObj.pw, (err, isMatch) => {
                if (err) {
                    res.json({ success: false, message: 'Server Error: Please try again later' });
                    throw err;
                }
                if (isMatch) {
                    acctObj._id = acctObj.cust.oid;
                    // log cust in by assigning them a token
                    const token = jwt.sign(acctObj, config.secret, {
                        expiresIn: 604800 // 1 week worth of seconds
                    });
                    done(err, acctObj, token);
                } else {
                    res.json({ success: false, message: 'Wrong password Entered.' });
                }
            });
        },
        function (acctObj, token, done) {
            // Get details of logged-in customer
            console.log('ACCOUNT OBJ');
            const dbRef = acctObj.cust;
            console.log(acctObj);
            console.log('ACCOUNT OBJ');
            Customer.getloggedInCustDetails(dbRef, (err, custDetails) => {
                if (err) throw err;
                console.log('CUST DETAILS');
                console.log(custDetails);
                console.log('CUST DETAILS');
                var respObj = { success: true, token: 'JWT ' + token, message: 'You have successfully logged in !' };
                if (custDetails.custType === 2) {
                    respObj.user = custDetails;
                    res.json(respObj);
                } else {
                    const dbRef = custDetails.min;
                    Ministry.getMinistryDetails(dbRef, (err, ministry) => {
                        if (err) throw err;
                        custDetails.mid = ministry._id;
                        // Remove ministry ID and list of depts frm ministry obj
                        delete ministry._id;
                        delete ministry.depts;
                        custDetails.min = ministry;
                        respObj.user = custDetails;
                        res.json(respObj);
                    });
                }

            });
        }
    ],
        function (err, result) {
            res.json({ success: false, message: 'Server Error: Please try again later' });
        });
});



// // Create a customer account
// router.post('/register', (req, res, next) => {
//   const addrObj = {
//       addr1: req.body.addr1,
//       addr2: req.body.addr2,
//       state: req.body.state,
//       country: req.body.country
//   };
//   const unverified_comp = {
//       compName: req.body.compName,
//       addr: addrObj,
//       verified: false
//   };
//   const newCust = {
//       fName: req.body.fName,
//       lName: req.body.lName,
//       mName: req.body.mName,
//       addr: addrObj,
//       compName: req.body.compName,
//       mobile: null,
//       work: req.body.work,
//       ext: req.body.ext,
//       email: req.body.email,
//       verified: false
//   };

//   async.waterfall([
//     // Add new customer to customers collection
//     function(done) {
//       Customer.addCustomer(newCust, (err, cust) => {
//         if (err) {
//           res.json({ success: false, message: err });
//         } else {
//           done(err, cust);
//         }
//       });
//     }, 
//     // Add new customer acct auth info to cust_auth collection
//     function(cust, done) {
//       const authObj = {
//         un: req.body.user,
//         pw: req.body.pw,
//         email: req.body.email,
//         passwordResetToken: null,
//         passwordExpirationDate: null,
//         cust: {
//           $ref: "customers",
//           $id: cust._id
//         }
//       };
//       Cust_Auth.addCustAuth(authObj, (err, authDetails) => {
//         if (err) throw err;
//         res.json({ success: true, message: 'Account registered successfully' });
//       });
//     }
//   ]);

//   async.waterfall([
//     // check if new customer's company exists in DB
//     function (done) {
//         Cust_Company.getCompanyByName(newCust.compName, (err, company) => {
//             if (err) {
//                 console.log('Error occured while searching for customer company from DB');
//             }
//             if (!company) {
//                 console.log('company does not exist in DB');
//                 // Add the unverified company to DB
//                 Cust_Company.addUnverifiedCompany(unverified_comp, (err, comp) => {
//                     if (err) throw err;
//                     console.log('unverified company successfully added');
//                 });
//             }
//             if (company) {
//                 done(err, company);
//             }
//         });
//     },
//     function (company, done) {
//         CustEmailModule.sendVerifEmailToCustComp(company, newCust, req.headers.host, (err) => {
//             if (err) throw err;
//             console.log('Verification Email has been sent to ' + company.compName);
//         });
//     }
// ]);
// });

// Get Customer Profile. This is a protected route. Hence the use of passport as a second param
router.get('/profile', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    res.json({ cust: req.user });
});

// Send username to customer 
router.post('/retrieve-un', (req, res, next) => {
    console.log('RRRRRRRRRRRR');
    console.log(req.body.email);
    console.log('RRRRRRRRRRRR');
    async.waterfall([
        function (done) {
            Customer.getCustomerByEmail(req.body.email, (err, cust) => {
                if (err) {
                    res.json({ success: false, message: 'Server Error: Please try again later ' });
                }
                if (!cust) {
                    res.json({ success: false, message: 'Email address not found in our records. Please enter the correct email address for your account.' });
                } else if(cust) {
                    done(err, cust);
                }
            });
        },
        // Get customer email from their auth obj and send them their username
        function (cust, done) {
            if (cust.lName.toUpperCase() === req.body.lName.toUpperCase()) {
                Cust_Auth.getCustomerAuthObjByEmail(cust.email, (err, custAuthObj) => {
                    if (err) throw err;
                    CustEmailModule.sendUnToCustomer(cust, custAuthObj.un, req.headers.host, (err) => {
                        if (err) {
                            res.json({ success: false, message: 'Server Error: Please try again later ' });
                        } else {
                            res.json({ success: true, message: 'Your username has been sent to ' + cust.email });
                        }
                    });
                });
            } else {
                res.json({ success: false, message: 'Invalid Last name / Email combination' });
            }
        }
    ]);
});

// Send a password reset link to Customer's email
router.post('/forgotpw', (req, res, next) => {
    async.waterfall([
        function (done) {
            // Generate a pw reset token
            crypto.randomBytes(20, (err, buf) => {
                let token = buf.toString('hex');
                done(err, token);
            });
        },
        function (token, done) {
            Cust_Auth.getCustomerAuthObjByEmail(req.body.pwResetEmail, (err, custAuthObj) => {
                if (err) {
                    res.json({ success: false, message: 'Server Error: Please try again later' });
                }
                if (!custAuthObj) {
                    res.json({ success: false, message: 'Please enter the correct email address for your account.' });
                    return;
                }
                if (custAuthObj && (custAuthObj.un.toUpperCase() === req.body.pwResetUn.toUpperCase())) {
                    //   token that gets appended to the password recovery link
                    //   Makes the pw token expire in 1hr
                    Cust_Auth.updateCustAuthInfo({ 'email': req.body.pwResetEmail }, {
                        $set: {
                            "passwordResetToken": token,
                            "passwordExpirationDate": Date.now() + 360000
                        }
                    }, () => {
                        done(err, token, custAuthObj);
                    });
                } else {
                    res.json({ success: false, message: 'Invalid user ID / email combination' });
                    return;
                }
            });
        },
        // Retrieve customer's info by their email. This is done just to retrieve their name to be included in the pw reset email sent out
        function (token, custAuthObj, done) {
            Customer.getCustomerByEmail(req.body.pwResetEmail, (err, cust) => {
                if (err) {
                    res.json({ success: false, message: 'Server Error: Please try again later' });
                }
                done(err, token, cust);
            });
        },
        // Send pw reset to link customer's email
        function (token, cust, done) {
            CustEmailModule.sendPwResetEmailToCustomer(cust, token, req.headers.host, (err) => {
                if (err) {
                    res.json({ success: false, message: 'Server Error: Please try again later' });
                }
                res.json({ success: true, message: 'Password reset link successfully sent to ' + cust.email + '!' })
            });
        }
    ], (err) => {
        if (err) throw err;
    });
});

// Reset customer's pw 
router.post('/resetpw', (req, res, next) => {
    Cust_Auth.getCustomerAuthObjByResetToken(req.body.token, (err, cust) => {
        if (err) {
            res.json({ success: false, message: 'Server Error' });
        }
        if (!cust) {
            res.json({ success: false, message: 'Sorry, token has expired. Please request a new password reset link' });
            return;
        }
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(req.body.newPw, salt, (err, hash) => {
                if (err) {
                    throw err;
                } else {
                    Cust_Auth.updateCustAuthInfo({ 'passwordResetToken': req.body.token }, { $set: { "pw": hash } }, (err) => {
                        if (!err) {
                            res.json({ success: true, message: 'Password update successful !' });
                        } else {
                            res.json({ success: false, message: 'Password update failed' });
                            return;
                        }
                    });
                }
            });
        });
    });
});

// Update customer's profile
router.post('/updateprofile', (req, res, next) => {
    console.log('CLIENT-SIDE REQUEST BODY');
    console.log(req.body);
    async.waterfall([
        function (done) {
            Customer.getCustomerById(req.body.id, (err, cust) => {
                if (err) {
                    console.log('Server Error: Please try again later.');
                } else {
                    done(err, cust);
                }
            });
        },
        function (cust, done) {
            console.log(cust);
            const password = req.body.currentPw;
            Customer.comparePasswords(password, cust.pw, (err, isMatch) => {
                if (err) {
                    throw err;
                    res.json({ success: false, message: 'SERVER ERROR: password comparison failed.' });
                }
                if (isMatch) {
                    async.waterfall([
                        function (done) {
                            if (req.body.newPw1) {
                                bcrypt.genSalt(10, (err, salt) => {
                                    bcrypt.hash(req.body.newPw1, salt, (err, hash) => {
                                        if (err) {
                                            throw err;
                                        } else {
                                            done(err, hash);
                                        }
                                    });
                                });
                            } else if (!req.body.newPw1) {
                                console.log('no new password');
                                done(err, cust.pw);
                            }
                        },
                        function (pw, done) {
                            Customer.updateCustomer({ '_id': ObjectId(req.body.id) },
                                {
                                    $set: {
                                        'fName': req.body.fName, 'mName': req.body.mName,
                                        'lName': req.body.lName, 'addr': req.body.addr,
                                        'compName': req.body.compName, 'mobile': req.body.mobile, 'work': req.body.work,
                                        'email': req.body.email,
                                        'pw': pw
                                    }
                                }, (err) => {
                                    if (err) {
                                        res.json({ success: false, message: 'Server Error: Profile update failed. Please try again later.' })
                                    }
                                    res.json({ success: true, message: 'User Profile updated successfully' });
                                });
                        }
                    ]);
                } else {
                    return res.json({ success: false, message: 'The Current Password entered is invalid' });
                }
            });
        }
    ]);
});

// // This applies to adding alt contacts to an EXISTING RFQ
// router.post('/addContact', (req, res, next) => {
//     // Checks to ensure a primary contact can't be added as an alternate contact
//     if (req.body.email == req.body.pri_contact) {
//         res.json({success: false, message: 'Failed Operation. You are the primary contact on this RFQ.'});
//         return;
//     } else {
//         async.waterfall([
//             function(done) {
//               // check first if the contact is verified
//           Customer.getCustomerByEmail(req.body.email, (err, cust) => {
//               if (err) throw err;
//               if (!cust) {
//                   res.json({success: false, message: 'No customer by that email was found. Please verify email.'});
//                   return;
//               }
//               if (!cust.verified) {
//                   console.log(cust.verified);
//                   console.log('cust not verified');
//                   res.json({success: false, message: 'This contact cannot be added as they have not yet been verified'});
//                   return;
//               } else {
//                   console.log('customer is verified');
//                   done(err, cust);
//               }
//           });
//               },
//               function(cust, done) {
//               // Check if this cust exists in the alt_contacts array of the rfq we adding contact to. Guides against duplicates
//               Rfq.getRfqByRfqSeq(req.body.rfqSeq, (err, rfqObj) => {
//                   if (err) throw err;
//                   if (rfqObj.alt_contacts.length > 0) {
//                       for (var i = 0; i < rfqObj.alt_contacts.length; i++) {
//                           if (rfqObj.alt_contacts[i].email == req.body.email) {
//                               res.json({success: false, message: 'Contact already exists on this RFQ'});
//                               return;
//                           } else {
//                               console.log('contact DOES NOT EXIST on rfq');
//                               done(err, rfqObj, cust);
//                               return;
//                           }
//                       }
//                   } else {
//                       done(err, rfqObj, cust);
//                   }
//               });
//               },
//               function(rfqObj, cust, done) {
//                   // Add the cust to the alt_contacts array of the rfq
//                   delete cust._id;
//                   console.log('****RFQOBJ****');
//                   console.log(rfqObj);
//                   console.log('****RFQOBJ****');
//                   console.log('((_____');
//                   console.log(cust);
//                   console.log('((_____');
//                   rfqObj.alt_contacts.push(cust);
//                   console.log(rfqObj.alt_contacts);
//                   Rfq.updateRfq({ '_id': ObjectId(rfqObj._id) }, { $set: { "alt_contacts": rfqObj.alt_contacts }}, (err) => {
//                       if (err) {
//                           res.json({success: false, message: 'Server Error: Please try again later'});
//                       }
//                       res.json({success: true, message: 'Contact successfully added'});
//                   });
//               }
//           ]);
//     }
// });

// Check if customer exists in the DB for a specific company
router.post('/getby_email', (req, res, next) => {
    console.log(req.body.email);
    Customer.getCustomerByEmail(req.body.email, (err, cust) => {
        if (err) throw err;
        if (!cust) {
            res.json({ success: false, message: 'No customer by that email was found' });
        } else if (cust) {
            res.json({ success: true, cust: cust });
        }
    });
});
module.exports = router;