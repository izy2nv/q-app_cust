const express = require('express');
const router = express.Router();
const Customer = require('../models/customer');
const Cust_Company = require('../models/cust_company');
const crypto = require('crypto');
const async = require('async');
const config = require('../config/db');
const MongoClient = require('mongodb').MongoClient;
const CustEmailModule = require('../emails/cust_emails');
const ObjectId = require('mongodb').ObjectID;
const Common = require('../models/common');
const RfqEmailModule = require('../emails/rfq_emails');

// Get all Chs by products cat

module.exports = router;