// Mailgun config for sending emails
var api_key = 'key-05ff46fdbc55a20993b2a29687412e8b';
var domain = 'sandboxe7580ba510e143d2ada6cfe84a0570c2.mailgun.org';
var mailgun = require('mailgun-js')({ apiKey: api_key, domain: domain });
var qApp = '<no_reply@quotesapp.com>';

module.exports.sendNewRfqEmailToCH = function (chList, newRfq, host, callback) {
  for (var i = 0; i < chList.length; i++) {
    var data = {
      from: qApp,
      to: chList[i].email,
      subject: 'NEW Request For Quotes #' + newRfq.rfqSeq,
      html: 'Hello, ' + '\n\
          <br />A new RFQ has been created.\n\
          <br/>Below is some information about this request.\n\
          <br /><strong>Date Created:</strong> '+ newRfq.createdOn + '\n\
          <br /><strong>RFQ ID: </strong>'+ newRfq.rfqSeq + '\n\
          <br /><strong>RFQ Tag: </strong>'+ newRfq.tag + '\n\
          <br /><strong>Details: </strong>'+ newRfq.desc + '\n\
          <br /><strong>RFQ Closes by: </strong>'+ new Date(newRfq.bidEndDate) + '\n\
          <br /><br/>Click ' + '<button><a href="http://' + host + '/login' + '">LOGIN</a></button> to log in to the application, to view more information on this rfq. \n\
          <br /><br />Thanks, <br />The Quotes Team.'
    };
    mailgun.messages().send(data, callback);
  }
};

module.exports.sendRfqCancellationEmailToCH = function (req, chList, host, callback) {
  console.log(rfqSeq);
  var data = {
    from: qApp,
    //   to: cust.email,
    subject: 'RFQ #' + req.rfqSeq + ' Cancellation',
    html: 'Hello, ' + '\n\
          <br />Below is your username, as requested. \n\
          <br /><button><a href="http://' + host + '/login' + '">CLICK TO LOGIN</a></button> \n\
          <br /><br />Thanks, <br />The Quotes Team.'
  };
  mailgun.messages().send(data, callback);
};

// Send email to alt contact abt being added to or deleted from an rfq.
module.exports.sendAddDelAltContEmail = function (rfqSeq, cont, flag, callback) {
  var subject, html;
  var line1 = 'Hello, ' + cont.fName + ' ' + cont.lName;
  if (flag) {
    subject = 'RFQ #' + rfqSeq + ': Alternate Contact Added';
    html = line1 + '\n\
      <br />This is to notify you that you were added as an alternate contact on RFQ ID: ' + rfqSeq + '. \n\
      <br />Please log-in to the Quotes App to see details about this RFQ. \n\
      <br /><br />Thanks, <br />The Quotes Team.';

  } else {
    subject = 'RFQ #' + rfqSeq + ': Alternate Contact Removed';
    html = line1 + '\n\
      <br />This is to notify you that you have been removed from the list of alternate contacts on RFQ ID: ' + rfqSeq + '. \n\
      <br />As a result, you can no longer view details of this RFQ. \n\
      <br /><br />Thanks, <br />The Quotes Team.';
  }
  var data = {
    from: qApp,
    to: cont.email,
    subject: subject,
    html: html
  };
  mailgun.messages().send(data, callback);
};