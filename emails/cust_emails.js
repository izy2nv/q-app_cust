// Mailgun config for sending emails
var api_key = 'key-05ff46fdbc55a20993b2a29687412e8b';
var domain = 'sandboxe7580ba510e143d2ada6cfe84a0570c2.mailgun.org';
var mailgun = require('mailgun-js')({ apiKey: api_key, domain: domain });
var qApp = '<no_reply@quotesapp.com>';

module.exports.sendVerifEmailToCustComp = function (estblshmnt, cust, host, callback) {
  console.log('estblshmnt to email is...');
  console.log(estblshmnt);
  console.log('estblshmnt to email is...');
  var html = null;
  if (estblshmnt.compName) {
    html = 'You are receiving this email because someone claiming to represent your interest in using our Quotes interface created an account.\n\
    <br />Information about them is as below: \n\
    <br /><div><br /><strong>Name:</strong> '+ cust.lName + ' ' + cust.mName + ' ' + cust.fName + '</div> \n\
    <br /><div><strong>Office Address:</strong> <br/>'+ cust.addr.addr1 + ' <br /> ' + cust.addr.addr2 + ' <br /> ' + cust.addr.state + ' <br /> ' + cust.addr.country + '</div> \n\
    <br /><div><strong>Office Number: </strong>'+ cust.work + ' &nbsp;<strong>Ext:</strong>' + cust.ext + '</div> \n\
    <br /><div><strong>Email: </strong>'+ cust.email + '</div> \n\
    <br />Please click on the CONFIRM or REJECT button below to either confirm them as legitimate representatives or reject their claim.\n\
    <br /><br /><button><a href="http://' + host + '/cust-verify/' + cust._id + 't' + '">CONFIRM</a></button> \n\
    <button><a href="http://' + host + '/cust-verify/' + cust._id + 'f' + '">REJECT</a></button> \n\
    <br /><br />Thanks, <br />The Quotes Team.';
  } if (estblshmnt.min) {
    html = 'You are receiving this email because someone claiming to represent your interest in using our Quotes interface created an account.\n\
    <br />Information about them is as below: \n\
    <br /><div><br /><strong>Name:</strong> '+ cust.lName + ' ' + cust.mName + ' ' + cust.fName + '</div> \n\
    <br /><div><strong>Ministry:</strong> '+ estblshmnt.min + ', ' + estblshmnt.state + ', ' + estblshmnt.country + '</div> \n\
    <br /><div><strong>Department / Unit:</strong> '+ cust.dept + '</div> \n\
    <br /><div><strong>Office Address:</strong> <br/>'+ cust.addr.addr1 + ' <br /> ' + cust.addr.addr2 + ' <br /> ' + cust.addr.state + ' <br /> ' + cust.addr.country + '</div> \n\
    <br /><div><strong>Office Number: </strong>'+ cust.work + ' &nbsp;<strong>Ext:</strong>' + cust.ext + '</div> \n\
    <br /><div><strong>Email: </strong>'+ cust.email + '</div> \n\
    <br />Please click on the CONFIRM or REJECT button below to either confirm them as legitimate representatives or reject their claim.\n\
    <br /><br /><button><a href="http://' + host + '/cust-verify/' + cust._id + 't' + '">CONFIRM</a></button> \n\
    <button><a href="http://' + host + '/cust-verify/' + cust._id + 'f' + '">REJECT</a></button> \n\
    <br /><br />Thanks, <br />The Quotes Team.';
  }
  var data = {
    from: qApp,
    to: estblshmnt.email,
    subject: 'Customer Verification',
    html: html
  };
  mailgun.messages().send(data, callback);
};

module.exports.sendUnToCustomer = function (cust, un, host, callback) {
  console.log(cust);
  var data = {
    from: qApp,
    to: cust.email,
    subject: 'Forgot Username',
    html: 'Hello ' + cust.lName + ' ' + cust.fName + ',' + '\n\
        <br />Below is your username, as requested. \n\
        <br /><div><br />'+ un + '</div> \n\
        <br /><button><a href="http://' + host + '/login' + '">CLICK TO LOGIN</a></button> \n\
        <br /><br />Thanks, <br />The Quotes Team.'
  };
  mailgun.messages().send(data, callback);
};

module.exports.sendPwResetEmailToCustomer = function (cust, token, host, callback) {
  console.log(cust);
  var data = {
    from: qApp,
    to: cust.email,
    subject: 'Password Reset',
    html: 'Hello ' + cust.lName + ' ' + cust.fName + ',' + '\n\
            <br />You are receiving this email because you (or someone else) requested for a password reset. \n\
            <br /> Please click the button below to reset your password. \n\
            <br /><br /><button><a href="http://' + host + '/pw-reset/' + token + '">RESET PASSWORD</a></button> \n\
            <br /><br />Thanks, <br />The Quotes Team.'
  };
  mailgun.messages().send(data, callback);
};

module.exports.sendCustVerificationEmail = function(cust, statusCode, host, callback) {
  var html = null;
  if (statusCode === 't') {
    html = 'Hello ' + cust.lName + ' ' + cust.fName + ',' + '\n\
    <br /> You have been successfully verified by '+ cust.compName + '.\n\
    <br /><button><a href="http://' + host + '/login' + '">CLICK TO LOGIN</a></button> \n\
    <br /><br />Thanks, <br />The Quotes Team.';
  } else if (statusCode === 'f') {
    html = 'Hello ' + cust.lName + ' ' + cust.fName + ',' + '\n\
    <br /> Your verification by '+ cust.compName + ' was not successful. As a result, you will not be able to represent the interests of '+ cust.compName + '.\n\
    <br /><br />Thanks, <br />The Quotes Team.';
  }
  var data = {
    from: qApp,
    to: cust.email,
    subject: 'Account Verification',
    html: html
  };
  mailgun.messages().send(data, callback);
};