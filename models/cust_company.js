const dbConfig = require('../config/db');
const ObjectId = require('mongodb').ObjectID;

// Get all verified companies
module.exports.getVerifiedCompanies = function (flag, callback) {
    dbConfig.db.cust_companies.find(flag, callback);
};

// Add unverified company
module.exports.addUnverifiedCompany = function(comp, callback) {
    dbConfig.db.cust_companies.save(comp, callback);
};

module.exports.getCompanyByName = function(name, callback) {
    const query = {compName: name};
    dbConfig.db.cust_companies.findOne(query, callback);
};

// Add verified customer as contact
module.exports.updateCompany = function(field, update, callback) {
    dbConfig.db.cust_companies.update(field, update, callback);
};

// // Remove (currently present) unverified customer from company's contacts list
// module.exports.removeCustFromCompContactsList = function(array, callback) {
//     dbConfig.db.cust_companies.remove({"books.title": "abc"});
// };