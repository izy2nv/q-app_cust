const bcrypt = require('bcryptjs');
const dbConfig = require('../config/db');
const ObjectId = require('mongodb').ObjectID;
const async = require('async');


// Get chs by passed param
module.exports.getAllChsByParam = function (param, callback) {
  console.log(param);
  dbConfig.db.chs.find(param, callback);
};

module.exports.getChDetails = function (chIdArray, callback) {
  dbConfig.db.chs.find({ "_id": {"$in": chIdArray} }, callback);
};

// Get a ch by ID
module.exports.getChById = function (id, callback) {
  console.log(id);
  dbConfig.db.chs.findOne(id, callback);
};