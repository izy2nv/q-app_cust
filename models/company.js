const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/db');

// Create a company schema
const CompanySchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    addr: {
        type: Object,
        required: true
    },
    contractId: {
        type: String,
        required: true
    },
    designation: {
        type: String,
        required: true
    },
    contacts: {
        type: Object
    },
    imgUrl: {
        type: String
    },
    link: {
        type: String
    },
    prodsCat: {
        type: Object
    }
});

const Company = module.exports = mongoose.model('Ch_company', CompanySchema);

// Get a user by ID from database
module.exports.getCompanyById = function (id, callback) {
    Company.findById(id, callback);
};