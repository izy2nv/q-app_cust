const dbConfig = require('../config/db');
const ObjectId = require('mongodb').ObjectID;
const async = require('async');
const bcrypt = require('bcryptjs');

module.exports.addCustAuth = function (cust, callback) {
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(cust.pw, salt, (err, hash) => {
      if (err) throw err;
      cust.pw = hash;
      dbConfig.db.cust_auth.save(cust, callback);
    });
  });
};

// Update customer's record
module.exports.updateCustAuthInfo = function(field, update, callback) {
  console.log(field);
  dbConfig.db.cust_auth.update(field, update, callback);
};

// Get a customer by email from db
module.exports.getCustomerAuthObjByEmail = function(email, callback) {
  const query = {email: email};
  dbConfig.db.cust_auth.findOne(query, callback);
};

// Get a customer by resetToken from db
module.exports.getCustomerAuthObjByResetToken = function(resetToken, callback) {
  const query = {passwordResetToken: resetToken};
  dbConfig.db.cust_auth.findOne(query, callback);
};