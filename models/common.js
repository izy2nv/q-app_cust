// This file holds re-usable codes that can be called in different files
const dbConfig = require('../config/db');
const ObjectId = require('mongodb').ObjectID;
const async = require('async');

module.exports.getObjectsDetails = function (objectsArray, collection, callback) {
  var arr = [];
  console.log('companies contacts Array');
  console.log(objectsArray);
  console.log('companies contacts Array');
  async.forEachOf(objectsArray, (value, key, err) => {
    arr.push(ObjectId(objectsArray[key].oid));
  });
  console.log('0000000');
  console.log(arr);
  console.log('0000000');
  dbConfig.db[collection].find({ "_id": { "$in": arr } }, callback);
};

module.exports.checkForObjInArray = function (val, array) {
  var isFound = array.some(function (el) {
    return el.oid === val;
  });
  return isFound;
};

module.exports.getNextSequenceValue = function(sequenceName, callback){
  dbConfig.db['counter'].findAndModify({query:{_id: sequenceName }, update: {$inc:{sequence_value:1}}, new: true}, callback);
};

module.exports.getStatesByCountry = function(country, callback) {
  dbConfig.db.user_countries.findOne(country, callback);
};

module.exports.getEstablishmentDetails = function (dbRef, callback) {
  dbConfig.db[dbRef.namespace].findOne({ "_id": ObjectId((dbRef.oid)) }, callback);
};

module.exports.getEstablishmentByParam = function(param, collection, callback) {
  console.log('testing');
  console.log(collection);
  console.log('testing');
  dbConfig.db[collection].findOne(param, callback);
};

// Get multiple customers by id from db
module.exports.getMultipleObjsByIdArray = function(idArray, collection, callback) {
 for (var i = 0; i < idArray.length; i++) {
  idArray[i] = ObjectId(idArray[i]);
 }
  dbConfig.db[collection].find({"_id" :{ $in: idArray}}, callback);
};

module.exports.processMultipleObjsArray = function (arrayOfEachObjPropVal, allObjsArr, field) {
  for (var i = 0; i < arrayOfEachObjPropVal.length; i++) {
    allObjsArr.some((el) => {
        if ((el[field].oid == arrayOfEachObjPropVal[i]._id) === true) {
          el[field] = arrayOfEachObjPropVal[i];
        }
      }); 
  }
  return allObjsArr;
};