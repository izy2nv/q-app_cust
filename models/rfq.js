const bcrypt = require('bcryptjs');
const dbConfig = require('../config/db');
const ObjectId = require('mongodb').ObjectID;
const async = require('async');

// Get all products/services categories
module.exports.getAllProdServCats = function(callback) {
  dbConfig.db.cats.find(callback);
};

// // Get all Servivies categories
// module.getAllProdsCats = function(callback) {
//   dbConfig.db.prod_cats.find(callback);
// };

// Save a new RFQ
module.exports.saveNewRfq = function (rfqObj, callback) {
  dbConfig.db.rfqs.save(rfqObj, callback);
};

// Get an rfq by rfqSeq ID
module.exports.getRfqByRfqSeq = function (rfqSeq, callback) {
  const query = { "rfqSeq": rfqSeq };
  dbConfig.db.rfqs.findOne(query, callback);
};

// Get an rfq by passed param
module.exports.getAllRfqsByPrimaryContact = function (param, callback) {
  console.log(param);
  dbConfig.db.rfqs.find(param, callback);
};

module.exports.getPriContactDetails = function (dbRef, callback) {
  console.log('PRIMARY CONTACT ID');
  console.log(dbRef.oid);
  console.log('PRIMARY CONTACT ID');
  dbConfig.db[dbRef.namespace].findOne({ "_id": ObjectId((dbRef.oid)) }, callback);
};



// module.exports.getAltcontactsDetails = function (rfqObjsArray, collection, callback) {
//   var rfqsWithoutAltContacts = [];
//   var rfqsWithAltContacts = [];
//   var alt;

//   for (var i=0; i < rfqObjsArray.length; i++) {
//     if (rfqObjsArray[i].alt_contacts.length === 0) {
//       rfqsWithoutAltContacts.push(rfqObjsArray[i]);
//     }
//     if (rfqObjsArray[i].alt_contacts.length > 0) {
//       // var retrievedAltContDetails = null;
//       // var retrievedAltContDetails = dbConfig.db[collection].find({ "_id": { "$in": rfqObjsArray[i].alt_contacts } });
//       alt = dbConfig.db['customers'].find({ "_id": { "$in": rfqObjsArray[i].alt_contacts } });
//       // console.log(alt);
//       alt.forEach(function(obj) {
//         console.log(obj);
//       });
//     }
//   }
//   console.log('OOOOOOOOOOO');
//   console.log(rfqsWithoutAltContacts);
//   console.log('OOOOOOOOOOO');

  
//   // dbConfig.db[collection].find({ "_id": { "$in": arr } }, callback);
// };

module.exports.getAltcontactsDetails = function (altContactsArray, collection, callback) {
  var arr = [];
  console.log('SHOW COLLECTIOJ');
  console.log(collection);
  console.log('SHOW COLLECTIOJ');
  async.forEachOf(altContactsArray, (value, key, err) => {
    arr.push(ObjectId(altContactsArray[key].oid));
  });
  console.log('0000000');
  console.log(arr);
  console.log('0000000');
  dbConfig.db[collection].find({ "_id": { "$in": arr } }, callback);
  // dbConfig.db[dbRef.namespace].findOne({"_id":ObjectId((dbRef.oid))}, callback);

};

module.exports.getAllRfqs = function (param, callback) {
  // console.log(param);
  // const objId = {"_id": ObjectId(obj.id)};
  // dbConfig.db.rfqs.find(param, {"alt_contacts":1}, callback);
  // dbConfig.db.rfqs.find(param, {"alt_contacts":1});
  dbConfig.db.rfqs.find(param, { "alt_contacts": 1 }, callback);
  // var result = dbConfig.db.rfqs.findOne({"pri_contact": objId},{"alt_contacts":1});

  // var result = dbConfig.db.rfqs.findOne({"pri_contact": objId},{"alt_contacts":1});
  // dbConfig.db.customers.find({"_id":{"$in":result["alt_contacts"]}}, callback);

};

module.exports.getAltContactsDetails = function (altContsArray, callback) {
  console.log('%%%%%%%');
  console.log(altContsArray);
  console.log('%%%%%%%');
  dbConfig.db.customers.find({ "_id": { "$in": altContsArray } }, callback);
};

module.exports.deleteRfqByRfqSeq = function(rfqSeq, callback) {
  const query = {"rfqSeq" : rfqSeq};
  dbConfig.db.rfqs.remove(query, callback);
};

module.exports.saveDeletedRfq = function(rfqObj, callback) {
  dbConfig.db.canceled_rfqs.save(rfqObj, callback);
};

// Update RFQ record by passed field
module.exports.updateRfq = function(field, update, callback) {
  console.log(field);
  dbConfig.db.rfqs.update(field, update, callback);
};

// // Update customer's record
// module.exports.updateCustomer = function(field, update, callback) {
//   console.log(field);
//   dbConfig.db.customers.update(field, update, callback);
// };

// module.exports.deleteCustomerById = function(id, callback) {
//   const query = {"_id" : id};
//   dbConfig.db.customers.remove(query, callback);
// };



// Get all rfqs for logged-in user
module.exports.getAllRfqsForLoggedInUser = function (param, callback) {
  // console.log(param);
  dbConfig.db.rfqs.find(param, callback);
};

module.exports.processPriContInRfqsArray = function (custArray, rfqsArray) {
  for (var i = 0; i < custArray.length; i++) {
    rfqsArray.some((el) => {
        if((el.pri_contact.oid == custArray[i]._id) === true) {
          el.pri_contact = custArray[i];
        }
      }); 
  }
  return rfqsArray;
};

// // Get rfqs by ministry/dept combination
// module.exports.getRfqsByMinAndDept = function(param, callback) {
//   dbConfig.db.gvt_rfqs.find(param, callback);
// };
