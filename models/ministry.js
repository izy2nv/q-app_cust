const dbConfig = require('../config/db');
const ObjectId = require('mongodb').ObjectID;

// // Get all verified companies
// module.exports.getVerifiedMinistries = function (callback) {
//   dbConfig.db.mins.find(callback);
// };

module.exports.getMinistries = function(param, callback) {
  dbConfig.db.ministries.find(param, callback);
};

module.exports.getMinistryDetails = function (dbRef, callback) {
  dbConfig.db[dbRef.namespace].findOne({ "_id": ObjectId((dbRef.oid)) }, callback);
};

// Add verified customer as contact
module.exports.updateMinistry = function(field, update, callback) {
  dbConfig.db.ministries.update(field, update, callback);
};

// // Add verified customer as contact
// module.exports.updateMinistry = function(field, update, callback) {
//     dbConfig.db.cust_companies.update(field, update, callback);
// };


// // Remove (currently present) unverified customer from company's contacts list
// module.exports.removeCustFromCompContactsList = function(array, callback) {
//     dbConfig.db.cust_companies.remove({"books.title": "abc"});
// };