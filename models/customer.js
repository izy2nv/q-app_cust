const bcrypt = require('bcryptjs');
const dbConfig = require('../config/db');
const ObjectId = require('mongodb').ObjectID;

// Get a customer by ID from database
module.exports.getCustomerById = function(id, callback) {
  const query = {"_id": ObjectId(id)};
  dbConfig.db.customers.findOne(query, callback);
};

module.exports.retriveCustomerById = function(obj, callback) {
  const query = {"_id": ObjectId(obj.oid)};
  dbConfig.db.customers.findOne(query, callback);
};

// Get customer's auth obj by username in the cust_auth collection
module.exports.getAcctObjByUsername = function(username, callback) {
  const query = {un: username};
  // console.log('********* CUSTOMER USERNAME *********');
  // console.log(query);
  // console.log('********* CUSTOMER USERNAME *********');
  dbConfig.db.cust_auth.findOne(query, callback);
};


module.exports.getloggedInCustDetails = function(dbRef, callback) {
  dbConfig.db[dbRef.namespace].findOne({"_id":ObjectId((dbRef.oid))}, callback);
};

// Get a customer by email from db
module.exports.getCustomerByEmail = function(email, callback) {
  const query = {email: email};
  dbConfig.db.customers.findOne(query, callback);
};

// Get multiple customers by email from db
module.exports.getMultipleCustomesByEmailArray = function(contactsArray, callback) {
  var emailArray = [];
  for (var i = 0; i < contactsArray.length; i++) {
    emailArray.push(contactsArray[i].email);
  }
  dbConfig.db.customers.find({"email":{ $in: emailArray}}, callback);
};

// Get multiple customers by id from db
module.exports.getMultipleCustomersByIdArray = function(contactsArray, callback) {
  // console.log('HGHGHGHGGHGHGHGHGHGH');
  // console.log(contactsArray);
  // console.log('HGHGHGHGGHGHGHGHGHGH');
 for (var i = 0; i < contactsArray.length; i++) {
   contactsArray[i] = ObjectId(contactsArray[i]);
 }
  dbConfig.db.customers.find({"_id" :{ $in: contactsArray}}, callback);
};

// Update customer's record
module.exports.updateCustomer = function(field, update, callback) {
  console.log(field);
  dbConfig.db.customers.update(field, update, callback);
};

// Save new customer info to customers colection
module.exports.addCustomer = function(newCust, callback) {
  dbConfig.db.customers.save(newCust, callback);
};

// 'hash' is the already existing hashed password in the DB
module.exports.comparePasswords = function (enteredPassword, hash, callback) {
  bcrypt.compare(enteredPassword, hash, (err, isMatch) => {
      if (err) throw err;
      callback(null, isMatch);
  });
};

module.exports.deleteCustomerById = function(id, callback) {
  const query = {"_id" : id};
  dbConfig.db.customers.remove(query, callback);
};