import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { equalValueValidator } from '../../services/equal-value.validator';
import { AppService } from '../../services/app.service';
import { AuthService } from '../../services/app.authService';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-pw-reset',
  templateUrl: './pw-reset.component.html',
  styleUrls: ['./pw-reset.component.css'],
  providers: [AppComponent]
})
export class PwResetComponent implements OnInit {
  pwResetForm: FormGroup; private pwResetToken: any;
  constructor(private fb: FormBuilder, private router: Router,
    private route: ActivatedRoute, private authService: AuthService,
  private appComponent: AppComponent) { }

  ngOnInit() {
    this.createPwResetForm();
    this.pwResetToken = this.route.snapshot.params;
  }

  createPwResetForm() {
    this.pwResetForm = this.fb.group({
      // newPw: [null, Validators.required]
      'pw': ['', [
        Validators.required,
        Validators.minLength(6)
        ]
      ],
      'confirmPassword': ['', [
        Validators.required,
        Validators.minLength(6)
        ]
      ]
    },
    {validator: equalValueValidator('pw', 'confirmPassword')}  // key is to validate on the form group
  );
  }

  resetPw(pwObj) {
    console.log(pwObj);
    const resetObj = {
      token: this.pwResetToken.id,
      newPw: pwObj.pw
    };
    this.authService.resetPw(resetObj).subscribe(
      data => {
        if (data.success) {
          console.log(data);
          this.appComponent.showSuccessMsg(data.message);
          this.pwResetForm.reset();
          this.router.navigate(['login']);
        } else {
          console.log('pw reset failed');
        }
      },
      err => {
        console.log(err);
      }
    );
    console.log(this.pwResetToken);
  }
}
