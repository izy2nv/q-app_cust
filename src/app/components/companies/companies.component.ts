import { Component, OnInit } from '@angular/core';
import { TableModule } from 'primeng/table';
import { AppService } from '../../services/app.service';
import { AppComponent } from '../../app.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css']
})
export class CompaniesComponent implements OnInit {
  companies = [];

  constructor(private appComponent: AppComponent, private appService: AppService,
  private router: Router) { }

  ngOnInit() {
    this.getAllCompanies();
  }

  getAllCompanies() {
    this.appService.getAllCompanies().subscribe(
      data => {
        this.companies = data;
      },
      err => {
        console.log(err);
      }
    );
  }

  getCompanyDetails(id) {
    this.appComponent.companyId = id;
    this.router.navigate(['companyDetails']);
  }
}
