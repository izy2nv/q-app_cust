import { Component, OnInit, ElementRef } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from '../../services/app.service';
import { AuthService } from '../../services/app.authService';
import { AppComponent } from '../../app.component';
import { HeaderComponent } from '../header/header.component';
// import { NgRedux } from '@angular-redux/store';
import { USER_LOGGEDIN } from '../../actions';
declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AppService, AuthService, AppComponent, HeaderComponent]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup; pwResetForm: FormGroup; unRecovForm: FormGroup; private isLoginView: boolean;
  private formPrompt: string; private isPwResetTrig: boolean; private modalTitle: string;
  private dom; private retMsg: string;

  constructor(private fb: FormBuilder, private router: Router, private elRef: ElementRef,
    private appService: AppService, private authService: AuthService,
    private appComponent: AppComponent, private headerComponent: HeaderComponent) {
    this.createLoginForm();
    this.createPwResetForm();
    this.createUnRecoveryForm();
  }

  ngOnInit() {
    this.dom = $(this.elRef.nativeElement);
    this.isLoginView = true;
    $('#loginModal').on('show.bs.modal', function (e) {
      this.isLoginView = true;
      $('#msgDiv').hide();
    });
  }

  createLoginForm() {
    this.loginForm = this.fb.group({
      user: [null, Validators.required],
      pw: [null, Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }

  createPwResetForm() {
    this.pwResetForm = this.fb.group({
      pwResetEmail: [null, Validators.required],
      pwResetUn: [null, Validators.required]
    });
  }

  createUnRecoveryForm() {
    this.unRecovForm = this.fb.group({
      lName: [null, Validators.required],
      email: [null, Validators.required]
    });
  }


  login(obj): void {
    this.authService.authenticateUser(obj).subscribe(
      data => {
        console.log(data);
        if (data.success) {
          this.authService.storeUserData(data.token, data.user);
          this.headerComponent.getCustVeriStatus();
          this.headerComponent.closeModal();
          this.router.navigate(['dashboard']);
        } else {
          this.showMsg(data.message, 'text-danger');
        }
      },
      err => {
        this.showMsg('Server Error. Please try again later.', 'text-danger');
      });
  }

  trigFp() {
    this.isLoginView = false;
    this.hideMsg();
    this.modalTitle = 'Reset Password';
    this.formPrompt = 'Please submit the form below. We\'ll e-mail you a link to reset your password.';
    this.isPwResetTrig = true;
    this.loginForm.reset();
    this.pwResetForm.reset();
  }

  trigFu() {
    this.isLoginView = false;
    this.hideMsg();
    this.modalTitle = 'Recover Username';
    this.formPrompt = 'Please submit the form below. We\'ll e-mail you your user ID.';
    this.isPwResetTrig = false;
    this.loginForm.reset();
    this.unRecovForm.reset();
  }

  requestPwReset(user) {
    this.authService.sendPwResetLinkToUser(user).subscribe(
      data => {
        console.log(data);
        if (data.success) {
          this.isLoginView = true;
          this.showMsg(data.message, 'text-success');
        } else {
          this.showMsg(data.message, 'text-danger');
        }
      },
      err => {
        console.log(err);
        this.showMsg('Server Error: Please try again later', 'text-danger');
      }
    );
  }
  requestUn(user) {
    this.authService.retriveUn(user).subscribe(
      data => {
        if (data.success) {
          this.isLoginView = true;
          this.showMsg(data.message, 'text-success');
        } else {
          this.showMsg(data.message, 'text-danger');
        }
      },
      err => {
        this.showMsg('Server Error: Please try again later', 'text-danger');
      }
    );
  }

  cancel() {
    this.isLoginView = false;
  }

  hideMsg() {
    $('#msgDiv').hide();
  }

  back() {
    this.isLoginView = true;
  }

  showMsg(msg, styleClass) {
    this.retMsg = msg;
    $('#msgDiv').show();
    setTimeout(() => {
      $('#msgDiv').addClass(styleClass);
    }, 10);
  }

  hideModal() {
    $('#loginModal').modal('hide');
  }
}

