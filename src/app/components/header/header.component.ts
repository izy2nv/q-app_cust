import { Component, OnInit, ElementRef } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from '../../services/app.service';
import { AuthService } from '../../services/app.authService';
import { AppComponent } from '../../app.component';
declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [AppService, AuthService, AppComponent]
})
export class HeaderComponent implements OnInit {
  private hLink; private hLinkTitle; public dom; public isVerified: boolean;
  public modalTitle: string; private isErrorRet: boolean; private errMsg: string;
  private name: string;
  // private officeNo: string; private email: string;

  constructor(private fb: FormBuilder, private router: Router, private elRef: ElementRef,
    private authService: AuthService, private appComponent: AppComponent) {}

  ngOnInit() {
    this.isErrorRet = false;
    this.dom = $(this.elRef.nativeElement);
    this.hLink = '../../../assets/images/home.png';
    this.hLinkTitle = 'Q-App Home';
    this.getCustVeriStatus();
    // this.officeNo = '+1 919-949-4224';
    // this.email = 'help@quotesapp.com';
  }

  // Login Section
  trigLogin() {
    this.modalTitle = 'Log In';
  }

  closeModal() {
    $('.modal').modal('hide');
  }

  logout() {
    this.authService.removeUserData();
    this.router.navigate(['']);
    this.isVerified = false;
    return false;
  }

  getCustVeriStatus() {
    const loggedInUser = JSON.parse(localStorage.getItem('user'));
    if (loggedInUser) {
      this.authService.getUserProfile().subscribe(
        data => {
          this.isVerified = data.cust.verified;
          return;
        }
      );
    } else {
      return;
    }
  }
}
