import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfqEditComponent } from './rfq-edit.component';

describe('RfqEditComponent', () => {
  let component: RfqEditComponent;
  let fixture: ComponentFixture<RfqEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfqEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfqEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
