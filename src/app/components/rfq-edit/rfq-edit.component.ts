import { Component, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from '../../services/app.service';
import { RfqService } from '../../services/rfq.service';
import { FileService } from '../../services/file.service';
import { AppComponent } from '../../app.component';
import { FileSelectDirective, FileUploader } from 'ng2-file-upload';
// import { select } from '@angular-redux/store';
import { NgRedux, select } from '@angular-redux/store';
import { DELETE_ALT_CONT } from '../../actions';

declare var $: any;
// const url = 'http://localhost:3000/api/rfq_file_upload';

@Component({
  selector: 'app-rfq-edit',
  templateUrl: './rfq-edit.component.html',
  styleUrls: ['./rfq-edit.component.css', '../new-rfq/new-rfq.component.css'],
  providers: [RfqService, AppService, FileService]
})
export class RfqEditComponent implements OnInit, AfterViewInit {
  @select() selected_rfq;
  private rfqObj; private isChangebidEndDateTrig: boolean;
  rfqForm: FormGroup; private rfqType: string; public categories; private subCats;
  private dom; private user; private altcontacts: Array<Object>;
  private isAltContMsg: boolean; private errMsg: string; attachmentList: any = []; successMsg: string;
  uploader: FileUploader; private hint: string; private currentDate: any; public rfqSeq: number;
  private isModal: boolean; private imgTitle: string; private isUpdateErr: boolean;
  private retrievedCats;

  constructor(private fb: FormBuilder, private rfqService: RfqService, private appService: AppService,
    private elRef: ElementRef, private appComponent: AppComponent, private router: Router,
    private route: ActivatedRoute, private fileService: FileService, private ngRedux: NgRedux<any>) {
    this.createRfqForm();
    this.uploader = new FileUploader({ url: this.appComponent.url });
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.attachmentList.push(JSON.parse(response));
    };
  }

  ngOnInit() {
    this.retrieveCats();
    this.isUpdateErr = false;
    this.isModal = false;
    this.user = JSON.parse(localStorage.getItem('user'));
    this.hint = 'HINT: Contractors respond better to well-detailed description of requests.';
    this.isChangebidEndDateTrig = false;
    this.rfqSeq = Number(this.route.snapshot.params.id);
    this.getRfq(this.rfqSeq);
    // this.initalizeSelectEl();
    this.dom = $(this.elRef.nativeElement);
    this.listenToSelectEv();
    this.dom.find('#textarea2').characterCounter();
  }
  retrieveCats() {
    this.retrievedCats = JSON.parse(localStorage.getItem('cats'));
  }

  checkRadioBtn(id) {
    setTimeout(() => {
      if (id == '#prods') {
        this.dom.find('#services').attr('disabled', true);
      } else if (id == '#services') {
        this.dom.find('#prods').attr('disabled', true);
      }
      this.dom.find(id).click();
    }, 10);
  }
  createRfqForm() {
    this.rfqForm = this.fb.group({
      tag: [null, Validators.required],
      rfqType: null,
      cat: '',
      subCat: '',
      other: '',
      desc: '',
      bidEndDate: null,
      modReason: [null, Validators.required]
    });
  }

  getRfq(id) {
    this.selected_rfq.subscribe(
      data => {
        this.rfqObj = data;
        console.log(this.rfqObj);
        // setTimeout(() => {
        //   M.updateTextFields();
        // }, 100);
        // this.initalizeSelectEl();
        this.rfqType = this.rfqObj.rfqType;
        if (this.rfqObj.rfqType == 'Products') {
          this.checkRadioBtn('#prods');
        } else {
          this.checkRadioBtn('#services');
        }
        this.setSubCatList();
      }
    );
    // this.rfqObj = this.appService.rfqObj;
    // this.altContact = [];
    // this.rfqArr = [];
    // const idParam = {rfqSeq: id};
    // this.rfqService.getRfqById(idParam).subscribe(
    //   data => {
    //     this.isDataRet = true;
    //     console.log(data);
    //     this.rfqObj = data.rfq;
    //     console.log(document.getElementById('selectEl'));
    //     // setTimeout(() => {
    //     //   M.updateTextFields();
    //     // }, 500);
    //     // this.initalizeSelectEl();
    //     if (this.rfqObj.rfqType == 'Products') {
    //       console.log('its products');
    //       this.checkRadioBtn('#prods');
    //     } else {
    //       console.log('its services');
    //       this.checkRadioBtn('#services');
    //     }
    //     this.setSubCatList();
    //     // this.isDataRet = true;
    //     // console.log(data);
    //     // this.pgTitle = 'RFQ #' + data.rfq.rfqSeq + ': ' + data.rfq.tag;
    //     // this.rfqObj = data.rfq;
    //     // this.altContact.push(data.rfq.pri_contact);
    //     // this.rfqArr.push(data.rfq);
    //     // this.attachmentList = this.rfqObj.attachmentList;
    //   },
    //   err => {
    //     // this.isDataRet = true;
    //     console.log(err);
    //   }
    // );
  }

  changeRfqClosingDate(e) {
    this.isChangebidEndDateTrig = true;
  }
  undoChangeRfqClosingDate(e) {
    this.isChangebidEndDateTrig = false;
  }

  submitEditedRfq(rfqObj) {
    rfqObj.rfqSeq = this.rfqSeq;
    if (rfqObj.bidEndDate === null) {
      delete rfqObj.bidEndDate;
    }
    console.log(rfqObj);
    this.rfqService.updateRfq(rfqObj).subscribe(
      data => {
        this.isUpdateErr = false;
        this.appComponent.showSuccessMsg(data.message);
        this.router.navigate(['quotes']);
      },
      err => {
        if (err.message === undefined) {
          this.errMsg = 'Server Error: Please try again later';
        } else {
          this.errMsg = err.message;
        }
        this.isUpdateErr = true;
      }
    );
    // rfqObj.attachmentList = this.attachmentList;
    // rfqObj.alt_contacts = this.altcontacts;
    // rfqObj.pri_contact = this.user._id,
    //   console.log(rfqObj);
    // this.rfqService.createNewRfq(rfqObj).subscribe(
    //   data => {
    //     console.log(data.message);
    //     this.appComponent.showSuccessMsg(data.message);
    //     this.router.navigate(['quotes']);
    //   },
    //   err => {
    //     this.appComponent.showErrorMsg(err);
    //   }
    // );
  }

  setRfqType(e) {
    this.dom.find('#selectEl').prop('selectedIndex', 0);
    this.rfqType = e.target.value;
    this.dom.find('.selLabel')[0].classList.remove('hide');
    if (this.rfqType === 'Products') {
      this.categories = this.retrievedCats[0].prods;
      // this.initalizeSelectEl();
    } else if (this.rfqType === 'Services') {
      this.categories = this.retrievedCats[0].services;
      // this.initalizeSelectEl();
    }
  }

  // initalizeSelectEl() {
  //   setTimeout(() => {
  //     const elems = this.dom.find('select').formSelect();
  //     M.FormSelect.init(elems, {});
  //   }, 5);
  // }

  listenToSelectEv() {
    const sel_Label = this.dom.find('.selLabel')[0],
      el = this.dom.find('#selectEl')[0];
    this.dom.find('#selectEl').on('change', function () {
      if (el.value.length > 0) {
        sel_Label.classList.add('hide');
      } else {
        sel_Label.classList.remove('hide');
      }
    });
  }

  onCatSelect(e) {
    console.log(e.target.value);
    for (let i = 0; i < this.categories.length; i++) {
      if (this.categories[i].name === e.target.value) {
        this.subCats = this.categories[i]['subCat'];
      }
    }
    // setTimeout(() => {
    //   const subCatSel = this.dom.find('#subCatSelectEl').formSelect();
    //   M.FormSelect.init(subCatSel, {});
    // }, 10);
  }

  toggleAltContactForm() {
    if (this.isAltContMsg) {
      this.isAltContMsg = false;
    }
    this.dom.find('#altContsDiv').slideToggle('slow');
  }

  addAltContact() {
    // const altContObj = {
    //   fName: this.dom.find('#contFname').val(),
    //   lName: this.dom.find('#contlname').val(),
    //   email: this.dom.find('#contEmail').val()
    // };
    // console.log(altContObj);
    // if (altContObj.fName == '' || altContObj.lName == '' || altContObj.email == '') {
    //   this.errMsg = 'Please fill all fields.';
    //   this.isAltContMsg = true;
    // } else {
    //   // Checks if the alt contact to be added to the rfq, exists in our records and also is verified.
    //   this.appService.getCustomerByEmail(altContObj).subscribe(
    //     data => {
    //       console.log(data);
    //       if (data.success) {
    //         if (data.cust.verified) {
    //           console.log('cust is verified');
    //           this.altcontacts.push(altContObj);
    //           this.isAltContMsg = true;
    //           this.errMsg = '';
    //           this.successMsg = 'Contact Added';
    //           this.emptyAltContField();
    //           console.log(this.altcontacts);
    //         } else {
    //           this.successMsg = '';
    //           console.log('cust NOT verified');
    //           this.errMsg = 'This contact cannot be added yet on this rfq, as they have not been verified yet by ' + data.cust.compName;
    //           this.isAltContMsg = true;
    //         }
    //         // this.errorMsg = ''
    //       } else {
    //         this.successMsg = '';
    //         this.errMsg = 'This contact cannot be added on this rfq, as they do not exist in our records';
    //         this.isAltContMsg = true;
    //       }
    //     },
    //     err => {
    //       this.errMsg = 'Server Error: Please try again later';
    //       this.isAltContMsg = true;
    //     }
    //   );
    // }
  }
  emptyAltContField() {
    this.dom.find('#contFname', '#contlname', '#contEmail').val('');
  }
  cancelAltContact() {
    this.dom.find('#altContsDiv').slideUp('slow');
  }

  navToPrevPg() {
    window.history.back();
  }

  setSubCatList() {
    if (this.rfqType === 'Products') {
      this.categories = [{ name: 'Auto', subCat: [{ name: 'Cars' }, { name: 'Trucks' }] }, {
        name: 'Computers',
        subCat: [{ name: 'Desktops' }, { name: 'Laptops' }]
      }, { name: 'Phones', subCat: [{ name: 'Mobile' }, { name: 'Landline' }] }];
    } else if (this.rfqType === 'Services') {
      this.categories = [{ name: 'Electrical', subCat: [{ name: 'Transformer' }, { name: 'Wires/Cords' }] },
      { name: 'Real Estate', subCat: [{ name: 'Land' }, { name: 'House' }] }, {
        name: 'Transportation',
        subCat: [{ name: 'Air' }, { name: 'Road' }]
      }];
    }
    for (let i = 0; i < this.categories.length; i++) {
      if (this.categories[i].name == this.rfqObj.cat) {
        this.subCats = this.categories[i]['subCat'];
      }
    }
  }

  ngAfterViewInit() {
    console.log(document.getElementById('selectEl'));
    console.log(this.dom.find('#selectEl'));
    // setTimeout(() => {
    //   M.updateTextFields();
    // }, 1000);


    // console.log('test');
    // console.log(this.dom.find('#selectEl'));
    // setTimeout(() => {
    //   M.updateTextFields();
    // }, 500);
  }

  displayImg(fileName) {
    this.imgTitle = fileName;
    this.isModal = true;
    this.fileService.downloadFile(fileName).subscribe(
      data => {
        console.log(data);
        const imageUrl = URL.createObjectURL(data);
        const img = document.getElementById('imgFile');
        img.addEventListener('load', () => URL.revokeObjectURL(imageUrl));
        (<HTMLImageElement>document.getElementById('imgFile')).src = imageUrl;
      },
      err => {
        console.error(err);
      }
    );
  }

  promptFileDel(file) {
    console.log(file);
  }

  promptContDel(e, cont) {
    e.target.style.display = 'none';
    const el = document.getElementById(cont.email);
    el.style.display = 'block';
  }
  undoAltContDel(contEmail) {
    const parent = document.getElementById(contEmail);
    (<HTMLElement>parent.previousElementSibling).style.display = 'block';
    parent.style.display = 'none';
  }

  deleteAltContact(contact) {
    const cont = {
      email: contact.email,
      rfqSeq: this.rfqSeq,
      isAdd: false
    };
    this.rfqService.updateRfqAltContacts(cont).subscribe(
      data => {
        if (data.success) {
          console.log(data.message);
          this.ngRedux.dispatch({ type: DELETE_ALT_CONT, payload: contact.email });
          // const index = this.rfqObj.alt_contacts.indexOf(contact);
          // this.rfqObj.alt_contacts = this.rfqObj.alt_contacts.filter((val, i) => i != index);
        } else {
          console.log(data.message);
        }
      },
      err => {
        this.errMsg = 'Server Error: Please try again later';
        this.isUpdateErr = true;
      }
    );
  }
}

