import { Component, OnInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/app.authService';
declare var $: any;

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css'],
  providers: [AuthService]
})
export class LandingPageComponent implements OnInit {
  private dom; parallaxImg1 = '../../../assets/images/pro1.jpg';
  private hints;

  constructor(private elRef: ElementRef) { }

  ngOnInit() {
    this.dom = $(this.elRef.nativeElement);
    // this.dom.find('.parallax').parallax();
    this.hints = [
      {iconClass: 'fa fa-sign-out', title: 'Request For Quotes', desc: 'Create a quote request. Allow us do the heavy-lifting !'},
      {iconClass: 'fa fa-file-text-o', title: 'Receive Multiple Quotes',
      desc: 'Compare quotes from multiple companies. Exchange correspondence on your quotes, via our Question/Answer tool'},
      {iconClass: 'fa fa-handshake-o', title: 'Initiate a handshake', desc: 'Experience an optimized handshake process.'}
    ];
  }

}
