import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/app.authService';
import { AppService } from '../../services/app.service';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, AfterViewInit {
  private fName: string; private mName: string;
  private lName: string; private company: string; private addr: string;
  private addr1; private addr2; private state; private country;
  private email: string; private work: number; private mobile: number;
  profileForm: FormGroup; private dom; private currentPw; private newPw1;
  private newPw2; custId: any; private isCustVerified: boolean;

  constructor(private authService: AuthService, private fb: FormBuilder,
    private appService: AppService, private appComponent: AppComponent) {
    this.createProfileForm();
  }

  ngOnInit() {
    this.getUserProfile();
  }

  getUserProfile() {
    this.authService.getUserProfile().subscribe(
      data => {
        console.log(data);
        if (!data.cust.verified) {
          this.isCustVerified = false;
        } else {
          this.isCustVerified = true;
        }
        this.custId = data.cust._id;
        this.processUserProfile(data.cust);
      },
      err => {
        console.log(err);
        return false;
      }
    );
  }

  processUserProfile(cust) {
    this.fName = cust.fName;
    this.lName = cust.lName;
    this.mName = cust.mName;
    this.company = cust.compName;
    this.addr1 = cust.addr.addr1;
    this.addr2 = cust.addr.addr2;
    this.state = cust.addr.state;
    this.country = cust.addr.country;
    this.addr = cust.addr;
    this.email = cust.email;
    this.work = cust.work;
    this.mobile = cust.mobile;
  }

  createProfileForm() {
    this.profileForm = this.fb.group({
      // compName: [this.company, Validators.required],
      addr1: [this.addr1, Validators.required],
      addr2: [this.addr2, Validators.required],
      state: [this.state, Validators.required],
      country: [this.country, Validators.required],
      work: this.work,
      mobile: this.mobile,
      currentPw: [this.currentPw, Validators.required],
      newPw1: this.newPw1,
      newPw2: this.newPw2
    });
  }

  updateProfile(updatedProfile) {
    updatedProfile.id = this.custId;
    this.appService.updateCustProfile(updatedProfile).subscribe(
      data => {
        if (data.success) {
          this.appComponent.showSuccessMsg(data.message);
          const el: HTMLElement = document.getElementById('profileEditCloseIcon') as HTMLElement;
          this.getUserProfile();
          el.click();
          console.log(data.message);
          console.log(data);
          this.profileForm.reset();
        } else {
          this.appComponent.showErrorMsg(data.message);
          console.log(data);
        }
      }
    );
  }
  ngAfterViewInit() {
    // setTimeout(() => {
    //   M.updateTextFields();
    // }, 1000);
  }
}
