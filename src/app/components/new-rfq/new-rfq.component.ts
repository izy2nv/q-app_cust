import { Component, OnInit, ElementRef } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from '../../services/app.service';
import { RfqService } from '../../services/rfq.service';
import { AppComponent } from '../../app.component';
import { FileSelectDirective, FileUploader } from 'ng2-file-upload';

declare var $: any;
const url = 'http://localhost:3000/api/rfq_file_upload';

@Component({
  selector: 'app-new-rfq',
  templateUrl: './new-rfq.component.html',
  styleUrls: ['./new-rfq.component.css'],
  providers: [RfqService, AppService]
})
export class NewRfqComponent implements OnInit {
  rfqForm: FormGroup; private rfqType: string; public categories; private subCats; private retrievedCats;
  private dom; private isCatSelected: boolean; private user; private altcontacts: Array<Object>;
  private isAltContMsg: boolean; private errMsg: string; attachmentList: any = []; successMsg: string;
  uploader: FileUploader = new FileUploader({ url: url }); private hint: string; private currentDate: any;
  private currencies: Array<Object>;


  constructor(private fb: FormBuilder, private rfqService: RfqService, private appService: AppService,
    private elRef: ElementRef, private appComponent: AppComponent, private router: Router) {
    this.createRfqForm();
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.attachmentList.push(JSON.parse(response));
    };
  }

  ngOnInit() {
    // this.retrieveCats();
    // this.currentDate = new Date();
    // this.isAltContMsg = false;
    // this.altcontacts = [];
    // this.user = JSON.parse(localStorage.getItem('user'));
    // this.isCatSelected = false;
    // this.dom = $(this.elRef.nativeElement);
    // this.listenToSelectEv();
    // this.dom.find('#textarea2').characterCounter();
    // this.checkProdRadioBtn();
    // const dateEl = this.dom.find('.datepicker');
    this.hint = 'HINT: Contractors respond better to well-detailed description of requests.';
    // this.currencies = [{name: 'NGN', icon: '&#8358;'}, {name: 'USD', icon: '&#36;'},
    // {name: 'GBP', icon: '&#163;'} , {name: 'EUR', icon: '&#8364;'}];
  }

  retrieveCats() {
    this.retrievedCats = JSON.parse(localStorage.getItem('cats'));
  }
  checkProdRadioBtn() {
    setTimeout(() => {
      this.dom.find('#prods').click();
    }, 10);
  }
  createRfqForm() {
    this.rfqForm = this.fb.group({
      tag: [null, Validators.required],
      rfqType: null,
      cat: '',
      subCat: '',
      other: '',
      desc: '',
      bidEndDate: [null, Validators.required]
    });
  }

  submitNewReq(rfqObj) {
    rfqObj.createdOn = new Date();
    rfqObj.attachmentList = this.attachmentList;
    rfqObj.alt_contacts = this.altcontacts;
    rfqObj.pri_contact = this.user._id;
    console.log(rfqObj);
    this.rfqService.createNewRfq(rfqObj).subscribe(
      data => {
        console.log(data.message);
        this.appComponent.showSuccessMsg(data.message);
        this.router.navigate(['quotes']);
      },
      err => {
        this.appComponent.showErrorMsg(err);
      }
    );
  }

  setRfqType(e) {
    this.dom.find('#selectEl').prop('selectedIndex', 0);
    this.rfqType = e.target.value;
    this.dom.find('.selLabel')[0].classList.remove('hide');
    if (this.rfqType === 'Products') {
      this.categories = this.retrievedCats[0].prods;
      // this.initializeSelectEl();
    } else if (this.rfqType === 'Services') {
      this.categories = this.retrievedCats[0].services;
      // this.initializeSelectEl();
    }
  }

  // initializeSelectEl() {
  //   setTimeout(() => {
  //     const elems = this.dom.find('select').formSelect();
  //     M.FormSelect.init(elems, {});
  //   }, 10);
  // }

  listenToSelectEv() {
    const sel_Label = this.dom.find('.selLabel')[0],
      el = this.dom.find('#selectEl')[0];
    this.dom.find('#selectEl').on('change', function () {
      if (el.value.length > 0) {
        sel_Label.classList.add('hide');
      } else {
        sel_Label.classList.remove('hide');
      }
    });
  }
  onCatSelect(e) {
    for (let i = 0; i < this.categories.length; i++) {
      if (this.categories[i].name === e.target.value) {
        this.subCats = this.categories[i]['subCat'];
      }
    }
    this.isCatSelected = true;
    // setTimeout(() => {
    //   const subCatSel = this.dom.find('#subCatSelectEl').formSelect();
    //   M.FormSelect.init(subCatSel, {});
    // }, 10);
  }

  onSubCatSelect(e) {
    const el = this.dom.find('.subCatLabel')[0];
    if (e.target.value === '') {
      el.classList.remove('hide');
    } else {
      el.classList.add('hide');
    }
  }

  toggleAltContactForm() {
    if (this.isAltContMsg) {
      this.isAltContMsg = false;
    }
    this.dom.find('#altContsDiv').slideToggle('slow');
  }

  addAltContact() {
    const altContObj = {
      fName: this.dom.find('#contFname').val(),
      lName: this.dom.find('#contlname').val(),
      email: this.dom.find('#contEmail').val()
    };
    console.log(altContObj);
    if (altContObj.fName == '' || altContObj.lName == '' || altContObj.email == '') {
      this.errMsg = 'Please fill all fields.';
      this.isAltContMsg = true;
    } else {
      // Checks if the alt contact to be added to the rfq, exists in our records and also is verified.
      this.appService.getCustomerByEmail(altContObj).subscribe(
        data => {
          console.log(data);
          if (data.success) {
            if (data.cust.verified) {
              console.log('cust is verified');
              this.altcontacts.push(altContObj);
              this.isAltContMsg = true;
              this.errMsg = '';
              this.successMsg = 'Contact Added';
              this.emptyAltContField();
              console.log(this.altcontacts);
            } else {
              this.successMsg = '';
              console.log('cust NOT verified');
              this.errMsg = 'This contact cannot be added on this rfq, as they have not been verified yet by ' + data.cust.compName;
              this.isAltContMsg = true;
            }
            // this.errorMsg = ''
          } else {
            this.successMsg = '';
            this.errMsg = 'This contact cannot be added on this rfq, as they do not exist in our records';
            this.isAltContMsg = true;
          }
        },
        err => {
          this.errMsg = 'Server Error: Please try again later';
          this.isAltContMsg = true;
        }
      );
    }
  }
  emptyAltContField() {
    this.dom.find('#contFname', '#contlname', '#contEmail').val('');
  }
  cancelAltContact() {
    this.dom.find('#altContsDiv').slideUp('slow');
  }

  navToPrevPg() {
    window.history.back();
  }
}
