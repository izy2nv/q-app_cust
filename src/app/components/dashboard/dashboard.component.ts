import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/app.authService';
import { AppService } from '../../services/app.service';
import { MessagesModule } from 'primeng/messages';
import { AppComponent } from '../../app.component';
import { Router } from '@angular/router';
import { RfqService } from '../../services/rfq.service';
import { NgRedux, select } from '@angular-redux/store';
import { SET_ALL_RFQS } from '../../actions';
declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [RfqService, AppComponent]
})
export class DashboardComponent implements OnInit {
  @select() allRfqs;
  private unverifCustInfo; private isCustVerified: boolean; private currentYr;
  private name: string; private user; private closedRfqs: number; private date;
  private years; private openRfqs: number; private isRfqsArrRet: boolean;
  // Doughnut chart
  public doughnutChartData: number[] = [null, null];
  public doughnutChartLabels: string[] = [null, null];
  public doughnutChartType = 'doughnut';
  public doughnutCharOptions = {
    cutoutPercentage: 75
  };
  public colors: Array<any> = [
    {
      backgroundColor: ['#7fc346', '#ff4c4c']
    }
  ];

  // bar chart
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData: any[] = [null, null];

  constructor(private authService: AuthService, private appService: AppService,
    private appComponent: AppComponent, private router: Router, private rfqService: RfqService,
    private ngRedux: NgRedux<any>) { }

  ngOnInit() {
    this.currentYr = new Date().getFullYear();
    this.date = new Date().toDateString();
    this.isRfqsArrRet = false;
    this.user = JSON.parse(localStorage.getItem('user'));
    this.unverifCustInfo = [];
    this.getUserProfile();
    // this.getAllRfqs();
    this.getAllProdServCats();
    this.doughnutChartData = [this.openRfqs, this.closedRfqs];
    this.years = [
      {name: '2018', val: 2018},
    {name: '2017', val: 2017},
    {name: '2016', val: 2016}
    ];
            // this.calcYrs(new Date().getFullYear() - 7);
  }

//   calcYrs(startYear) {
//     const currentYear = new Date().getFullYear(), years = [];
//     startYear = startYear || 1980;

//     while ( startYear <= currentYear ) {
//             years.push((startYear++).toString());
//     }
//     console.log(years);
//     return years;
// }

  getUserProfile() {
    this.authService.getUserProfile().subscribe(
      data => {
        console.log(data);
        this.name = data.cust.lName + ' ' + data.cust.fName;
        if (!data.cust.verified) {
          this.isCustVerified = false;
          let compName;
          if (data.cust.custType === 1) {
            compName = data.cust.min.min;
          } else if (data.cust.custType === 2) {
            compName = data.cust.compName;
          }
          this.unverifCustInfo.push({
            severity: 'info', detail: 'A verification email has been sent to ' + compName + '. ' +
            'Certain functionalities such as request for quotes will be \n\
            unavailable to you until you have been verified.'});
        } else {
          this.isCustVerified = true;
          this.getAllRfqs();
        }
      },
      err => {
        console.log(err);
        return false;
      }
    );
  }

  //    // Get all rfqs for logged-in user
  getAllRfqs() {
    let param;
    if (this.user.custType === 1) {
      console.log('govt user');
      param = {min: this.user.mid, dept: this.user.dept};
    } else if (this.user.custType === 2)  {
      console.log('Private company user');
    } else {
      console.log('individual user');
      param = { id: this.user._id };
    }
    this.rfqService.getAllRfqsBySearchParam(param).subscribe(
      data => {
        this.ngRedux.dispatch({ type: SET_ALL_RFQS, allRfqs: data.rfqsArray });
        if (data.rfqsArray.length > 0) {
          this.isRfqsArrRet = true;
          setTimeout(() => {
            this.animateValue('totalCount', 0, data.rfqsArray.length, 500);
          }, 10);
          this.processRfqData();
        } else {
          this.isRfqsArrRet = false;
        }
      },
      err => {
        // Handle cases of server issue. Hint: Either the table doesn't show or the error msg should b displayed on the table
        console.log(err);
      }
    );
  }

  animateValue(id, start, end, duration) {
    const obj = <HTMLElement>document.getElementById(id);
    const range = end - start;
    let current = start;
    const increment = end > start ? 1 : -1;
    const stepTime = Math.abs(Math.floor(duration / range));
    const timer = setInterval(() => {
      current += increment;
      obj.innerText = current;
      if (current === end) {
        clearInterval(timer);
      }
    }, stepTime);
  }

  getRfqsMonthsArr(data, date) {
    const rfqsArr = [];
    for (let i = 0; i < data.length; i++) {
      if (new Date(data[i].createdOn).getFullYear() == date) {
        rfqsArr.push(data[i]);
      }
    }
    const monthsArr = [];
    for (let i = 0; i < 12; i++) {
      const count = rfqsArr.filter((rfqObj) => (new Date(rfqObj.createdOn)).getMonth() === i).length;
      monthsArr.push(count);
    }
    return monthsArr;
  }

  processRfqData() {
    this.allRfqs.subscribe(
      data => {
        // doughnut chart
        let closed_count = 0;
        for (const rfq of data) {
          if (rfq.status === 2) {
            closed_count = closed_count + 1;
          }
        }
        this.closedRfqs = closed_count;
        this.openRfqs = data.length - closed_count;
        this.doughnutChartData[0] = this.openRfqs;
        this.doughnutChartData[1] = this.closedRfqs;
        this.doughnutChartLabels[0] = 'Open: ' + this.openRfqs;
        this.doughnutChartLabels[1] = 'Closed: ' + this.closedRfqs;

        // bar chart
        this.barChartData = [{data: this.getRfqsMonthsArr(data, this.currentYr), label: 'All RFQ\'s'},
        {data: [28, 48, 40, 19, 86, 27, 90], label: 'Awarded Contracts'}];
      }
    );
  }

  public changeBarChart(e) {
    this.allRfqs.subscribe(
      data => {
        this.barChartData = [{data: this.getRfqsMonthsArr(data, e.value.val), label: 'All RFQ\'s'},
        {data: [45, 10, 0, 30, 20, 47, 100], label: 'Awarded Contracts'}];
      }
    );
  }

  getAllProdServCats() {
    if (localStorage.getItem('cats') !== null) {
      return;
    } else {
      this.rfqService.getAllProdServCats().subscribe(
        data => {
          localStorage.setItem('cats', JSON.stringify(data.categories));
        },
        err => {
          console.log(err);
        }
      );
    }
  }
}
