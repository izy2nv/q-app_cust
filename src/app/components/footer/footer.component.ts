import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../../services/app.service';
import { error } from 'selenium-webdriver';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
  providers: [AppService]
})
export class FooterComponent implements OnInit {
private links_learn: any; private links_co: any; private links_su: any; private links_soc: any;
  constructor(private router: Router, private appService: AppService) { }

  ngOnInit() {
    this.getFooterLinks();
  }

  //Retrieve links for footer
  getFooterLinks() {
    this.appService.getFooterLinks().subscribe(
      data => {
        console.log(data);
        this.links_co = data[0]['company'];
        this.links_learn = data[0]['learn'];
        this.links_soc = data[0]['social'];
        this.links_su = data[0]['support'];
      },
      err => {
        console.log(err);
      }
    );
  }
  trigEmail() {
    console.log('email');
  }
  loadWP() {
    this.router.navigate(['white-paper']);
  }
  loadVids() {
    this.router.navigate(['videos']);
  }

}
