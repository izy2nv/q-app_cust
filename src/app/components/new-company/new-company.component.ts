import { Component, OnInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from '../../services/app.service';
declare var $: any;


@Component({
  selector: 'app-new-company',
  templateUrl: './new-company.component.html',
  styleUrls: ['./new-company.component.css']
})
export class NewCompanyComponent implements OnInit {
  private compForm: FormGroup; private countries; private states;
  private dom;

  constructor(private fb: FormBuilder, private router: Router, private appService: AppService,
    private elRef: ElementRef) {
      this.createCompForm();
     }

  ngOnInit() {
    this.dom = $(this.elRef.nativeElement);
    this.countries = [
      'Ghana', 'Nigeria', 'South Africa'
    ];
    // setTimeout(() => {
    //   const elems = this.dom.find('select').formSelect();
    //   M.FormSelect.init(elems, {});
    // }, 10);
  }

  createCompForm() {
    this.compForm = this.fb.group({
      country: '',
      compName: [null, Validators.required],
      rcNo: '',
      addr1: '',
      addr2: '',
      state: '',
      email: '',
      website: '',
      work: '',
      fax: '',
      ext: ''
    });
  }

  registerComp(comp) {
    console.log(comp);
  }
  // activateSelEl() {
  //   setTimeout(() => {
  //     const elems = this.dom.find('select').formSelect();
  //     M.FormSelect.init(elems, {});
  //   }, 10);
  // }

  selectCountry(e) {
    console.log(e);
    const param = {country: e.target.value};
    this.appService.getStatesByCountry(param).subscribe(
      data => {
        if (data.success) {
          console.log(data);
          this.states = data.states;
          // this.activateSelEl();
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  back() {
    window.history.back();
  }
}
