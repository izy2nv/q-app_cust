import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustVerificationComponent } from './cust-verification.component';

describe('CustVerificationComponent', () => {
  let component: CustVerificationComponent;
  let fixture: ComponentFixture<CustVerificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustVerificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustVerificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
