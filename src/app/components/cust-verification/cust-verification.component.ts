import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from '../../services/app.service';
import { MessagesModule } from 'primeng/messages';

@Component({
  selector: 'app-cust-verification',
  templateUrl: './cust-verification.component.html',
  styleUrls: ['./cust-verification.component.css']
})
export class CustVerificationComponent implements OnInit {
  private param; private verifStatusMsg; private verifRespRet: boolean;

  constructor(private router: Router, private route: ActivatedRoute, private appService: AppService) { }

  ngOnInit() {
    this.verifRespRet = false;
    this.verifStatusMsg = [];
    this.param = this.route.snapshot.params;
    this.verifyCustomer(this.param.id);
  }
  verifyCustomer(id) {
    this.appService.verifyCustomer(id).subscribe(
      data => {
        if (data.success) {
          this.verifStatusMsg.push({severity: 'success', summary: 'success', detail: data.message});
          this.verifRespRet = true;
        } else {
          this.verifStatusMsg.push({severity: 'error', summary: 'Error', detail: data.message});
          this.verifRespRet = true;
        }
      },
      err => {
        console.log(err);
      }
    );
  }
}

