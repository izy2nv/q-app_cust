import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { FileService } from '../../services/file.service';
import { RfqService } from '../../services/rfq.service';
import { MessagesModule } from 'primeng/messages';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-rfq',
  templateUrl: './rfq.component.html',
  styleUrls: ['./rfq.component.css'],
  providers: [RfqService, FileService]
})
export class RfqComponent implements OnInit {
  public rfqSeq: number; private rfqObj; pgTitle: string; private attachmentList: any;
  private isDataRet: boolean; private altContacts: Array<Object>; private pri_contact;
  constructor(private fb: FormBuilder, private router: Router, private fileService: FileService,
    private route: ActivatedRoute, private rfqService: RfqService) { }

  ngOnInit() {
    this.isDataRet = false;
    this.rfqSeq = Number(this.route.snapshot.params.id);
    this.getRfq(this.rfqSeq);
  }

  getRfq(id) {
    this.pri_contact = [];
    const idParam = {rfqSeq: id};
    this.rfqService.getRfqById(idParam).subscribe(
      data => {
        this.isDataRet = true;
        console.log(data);
        this.pgTitle = 'RFQ #' + data.rfq.rfqSeq + ': ' + data.rfq.tag;
        this.rfqObj = data.rfq;
        this.pri_contact.push(data.rfq.pri_contact);
        this.altContacts = data.rfq.alt_contacts;
        this.attachmentList = this.rfqObj.attachmentList;
      },
      err => {
        this.isDataRet = true;
        console.log(err);
      }
    );
  }

  // getRfq(id) {
  //   this.altContact = [];
  //   this.rfqArr = [];
  //   const idParam = {rfqSeq: id};
  //   this.rfqService.getRfqById(idParam).subscribe(
  //     data => {
  //       this.isDataRet = true;
  //       console.log(data);
  //       this.pgTitle = 'RFQ #' + data.rfq.rfqSeq + ': ' + data.rfq.tag;
  //       this.rfqObj = data.rfq;
  //       this.altContact.push(data.rfq.pri_contact);
  //       this.rfqArr.push(data.rfq);
  //       this.attachmentList = this.rfqObj.attachmentList;
  //     },
  //     err => {
  //       this.isDataRet = true;
  //       console.log(err);
  //     }
  //   );
  // }

  downloadFile(fileName) {
    console.log(fileName);
    this.fileService.downloadFile(fileName).subscribe(
      data => {
        console.log(data);
        saveAs(data, fileName);
      },
      err => {
        console.error(err);
      }
    );
  }

  editRfq() {
    console.log('editing...');
    this.router.navigate(['rfq/' + this.rfqSeq + '/edit']);
  }

}
