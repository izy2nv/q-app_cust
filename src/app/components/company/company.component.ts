import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../../app.component';
import { AppService } from '../../services/app.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {
  private dataRet: boolean; private comp = [];

  constructor(private appComponent: AppComponent, private appService: AppService) { }

  ngOnInit() {
    this.dataRet = false;
    this.getCompanyDetail();
  }

  getCompanyDetail() {
    this.appService.getCHcompanyById(this.appComponent.companyId).subscribe(
      data => {
        this.comp.push(data);
        this.dataRet = true;
      },
      err => {
        console.log(err);
      }
    );
  }

  emailContact() {

  }

  formatPhoneNo(no) {
    return this.appComponent.formatPhoneNo(no);
  }

  printPage() {
    window.print();
  }
}
