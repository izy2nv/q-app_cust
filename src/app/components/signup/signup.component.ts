import { Component, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from '../../services/app.service';
import { LoginComponent } from '../login/login.component';
import { AppComponent } from '../../app.component';
import { HeaderComponent } from '../header/header.component';

declare var $: any;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [AppService, LoginComponent, AppComponent, HeaderComponent]
})
export class SignupComponent implements OnInit, AfterViewInit {
  private signUpForm: FormGroup; private gvt_signUpForm: FormGroup; private compsArray; private dom;
  private errMsg: string; private ministries; private departments; private countries; private states;
  private isErrorRet: boolean; private isModal: boolean; private custType; private country;
  constructor(private fb: FormBuilder, private router: Router, private appService: AppService,
    private loginComponent: LoginComponent, private appComponent: AppComponent, private elRef: ElementRef) {
    this.createSignUpForm();
    this.createGvtSignUpForm();
  }

  ngOnInit() {
    this.countries = [
      'Ghana', 'Nigeria', 'South Africa'
    ];
    this.departments = [];
    this.ministries = [];
    this.isModal = true;
    this.isErrorRet = false;
    this.dom = $(this.elRef.nativeElement);
    this.listenToSelectEv();
  }

  getMinistries(e) {
    const param = { country: this.country, state: e.target.value };
    this.appService.getMinistries(param).subscribe(
      data => {
        console.log(data);
        this.ministries = data.ministries;
        // this.activateSelEl();
      },
      err => {
        console.log('An error occured');
      }
    );
  }

  selectMinistry(e) {
    for (let i = 0; i < this.ministries.length; i++) {
      if (this.ministries[i].min === e.target.value) {
        this.departments = this.ministries[i].depts;
      }
    }
  }

  getVerifiedCustCompanies() {
    const flag = { verified: true };
    this.appService.getAllVerifiedCustCompanies(flag).subscribe(
      data => {
        console.log(data);
        this.compsArray = data;
      },
      err => {
        console.log('An error occured');
      }
    );
  }
  signUp(newCust) {
    console.log(newCust);
    this.appService.registerNewCust(newCust).subscribe(
      data => {
        if (data.success) {
          this.loginComponent.login(newCust);
        } else {
          this.errMsg = data.message;
          this.isErrorRet = true;
        }
        console.log(data);
      },
      err => {
        console.log(err);
      }
    );
  }

  createSignUpForm() {
    this.signUpForm = this.fb.group({
      fName: [null, Validators.required],
      mName: '',
      lName: '',
      email: '',
      compName: [null, Validators.required],
      dept: '',
      user: '',
      pw: '',
      work: '',
      ext: '',
      addr1: '',
      addr2: '',
      state: '',
      country: ''
      // pw: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
    });
  }

  createGvtSignUpForm() {
    this.gvt_signUpForm = this.fb.group({
      fName: [null, Validators.required],
      mName: '',
      lName: '',
      email: '',
      min: [null, Validators.required],
      dept: [null, Validators.required],
      user: '',
      pw: '',
      work: '',
      ext: '',
      addr1: '',
      addr2: '',
      state: '',
      country: ''
      // pw: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
    });
  }

  toggleSelectElState(ev) {
    const elVal = ev.target.value,
      selectEl = this.dom.find('#selectEl')[0];
    if (elVal.length > 0) {
      selectEl.disabled = true;
    } else {
      selectEl.disabled = false;
    }
  }

  listenToSelectEv() {
    const sel_Label = this.dom.find('.selLabel')[0],
      el = this.dom.find('#selectEl')[0],
      compNameInputEl = this.dom.find('#comp')[0];
    this.dom.find('#selectEl').on('change', function () {
      if (el.value.length > 0) {
        sel_Label.classList.add('hide');
        compNameInputEl.disabled = true;
      } else {
        sel_Label.classList.remove('hide');
        compNameInputEl.disabled = false;
      }
    });
  }
  ngAfterViewInit() {
    // setTimeout(() => {
    //   const elems = this.dom.find('select').formSelect();
    //   M.FormSelect.init(elems, {});
    // }, 10);
    // this.dom.find('select').formSelect();
    // this.getVerifiedCustCompanies();
    // this.listenToSelectEv();
    // // this.getVerifiedCustCompanies();
  }

  setActType(e) {
    this.custType = Number(e.target.value);
    this.isModal = false;
    // this.activateSelEl();
  }

  back() {
    window.history.back();
  }

  selectCountry(e) {
    console.log(e);
    this.country = e.target.value;
    const param = { country: this.country };
    this.appService.getStatesByCountry(param).subscribe(
      data => {
        if (data.success) {
          console.log(data);
          this.states = data.states;
          // this.activateSelEl();
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  cancelSignUp() {
    this.gvt_signUpForm.reset();
  }
}
