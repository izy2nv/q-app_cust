import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RfqService } from '../../services/rfq.service';
import { AppComponent } from '../../app.component';
import { AppService } from '../../services/app.service';
import { NgRedux, select } from '@angular-redux/store';
import {
  EDIT_RFQ, ARCHIVE_RFQ, UNARCHIVE_RFQ, SET_ALL_RFQS, DELETE_RFQ,
  RESTORE_RFQ
} from '../../actions';
import {Table} from 'primeng/table';
import { DataTable } from 'primeng/primeng';

declare var $: any;
@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css'],
  providers: [RfqService, AppComponent, AppService]
})
export class QuotesComponent implements OnInit {
  @select() archived_rfqs;
  @select() unArchived_rfqs;
  // @select() allRfqs;
  @select() deleted_rfqs;
  @select() restored_rfq;
  public rfqsArray; private dom; private selectedRfqObj; public rfqID;
  private addContactForm: FormGroup; public selectedRfqAltContacts; private user;
  private emptyRfqMsg: string; private emptyAltContMsg: string; private modalTitle: string;
  private isModal: boolean; private prompt: string; private isDelRfq: boolean; private closeRfqErrMsg: boolean;
  private rfqCancelPrompt: string; private isAltContsClicked: boolean;
  private isDifferentUserFound: boolean; private diffUserFoundMsg: string; private diffUserFound;
  private retMsg: string; private isDataRet: boolean; private archivedRfqs; private unArchivedRfqs;
  private deletedRfqs;
  private isTablePaginated: boolean; private noOfRows: number;

  constructor(private fb: FormBuilder, private elRef: ElementRef,
    private rfqService: RfqService, private router: Router, private appComponent: AppComponent,
    private appService: AppService, private ngRedux: NgRedux<any>) {
    this.createAddContForm();
  }

  ngOnInit() {
    this.isTablePaginated = false;
    this.archivedRfqs = [];
    this.isDataRet = false;
    this.retMsg = '';
    this.isDifferentUserFound = false;
    this.rfqCancelPrompt = 'Please enter a reason for cancelling this RFQ.';
    this.isDelRfq = false;
    this.isAltContsClicked = false;
    this.isModal = false;
    this.emptyRfqMsg = 'No records found.';
    this.emptyAltContMsg = 'No Alternate contact(s) on this request.';
    this.user = JSON.parse(localStorage.getItem('user'));
    this.dom = $(this.elRef.nativeElement);
    const elem = this.dom.find('.modal');
    elem.modal({
      onCloseStart: function () {
        document.getElementById('addContForm').removeAttribute('style');
      }
    });
    // this.dom.find('.collapsible').collapsible();
    this.getAllRfqs();
  }

  onPaginate(dt: Table) {
    this.isTablePaginated = true;
    console.log(dt.totalRecords);


    // if (dt.rangeRowIndex !== null) {
    //   this.noOfRows = dt.dataToRender.length;
    //   console.log(this.noOfRows);
    // } else {
    //   return;
    // }
  }

  // Get all rfqs for logged-in user
  getAllRfqs() {
    // const idObj = { id: this.user._id };
    let param;
    if (this.user.custType === 1) {
      console.log('Govt user');
      param = {min: this.user.mid, dept: this.user.dept};
    } else if (this.user.custType === 2) {
      console.log('Private company user');
    } else if (this.user.custType === 3) {
      console.log('individual user');
      param = { id: this.user._id };
    }
    this.rfqService.getAllRfqsBySearchParam(param).subscribe(
      data => {
        this.isDataRet = true;
        // Update state
        this.ngRedux.dispatch({ type: SET_ALL_RFQS, allRfqs: data.rfqsArray });
        this.processAllRfqsData();
      },
      err => {
        console.log(err);
      }
    );
  }

  processAllRfqsData() {
    this.archived_rfqs.subscribe(
      data => {
        this.archivedRfqs = data;
      }
    );
    this.unArchived_rfqs.subscribe(
      data => {
        this.unArchivedRfqs = data;
      }
    );
    this.deleted_rfqs.subscribe(
      data => {
        this.deletedRfqs = data;
      }
    );
  }

  archiveRfq() {
    const arch_rfq = {
      rfqSeq: this.selectedRfqObj.rfqSeq,
      state: 2
    };
    this.rfqService.updateRfq(arch_rfq).subscribe(
      data => {
        this.ngRedux.dispatch({ type: ARCHIVE_RFQ, archived_rfq: this.selectedRfqObj });
      },
      err => {
        console.log(err);
      }
    );
  }
  unArchiveRfq() {
    const unArch_rfq = {
      rfqSeq: this.selectedRfqObj.rfqSeq,
      state: 1
    };
    const action = { type: UNARCHIVE_RFQ, unArchived_rfq: this.selectedRfqObj };
    this.updateRfq(unArch_rfq, action);
  }

  updateRfq(updateObj, action) {
    this.rfqService.updateRfq(updateObj).subscribe(
      data => {
        this.ngRedux.dispatch(action);
      },
      err => {
        console.log(err);
      }
    );
  }

  deleteRfq() {
    const del_rfq = {
      rfqSeq: this.selectedRfqObj.rfqSeq,
      state: 3
    };
    const action = { type: DELETE_RFQ, deleted_rfq: this.selectedRfqObj };
    this.updateRfq(del_rfq, action);
  }

  restoreRfq() {
    const restored_rfq = {
      rfqSeq: this.selectedRfqObj.rfqSeq,
      state: 1
    };
    const action = { type: RESTORE_RFQ, restored_rfq: this.selectedRfqObj };
    this.updateRfq(restored_rfq, action);
  }

  viewAltContacts(rfqObj) {
    this.modalTitle = 'Alternate Contacts on RFQ #: ' + rfqObj.rfqSeq;
    this.isAltContsClicked = true;
    this.isModal = true;
    console.log(rfqObj);
    // this.selectedRfqObj = rfqObj;
    this.rfqID = rfqObj.rfqSeq;
    this.selectedRfqAltContacts = rfqObj.alt_contacts;
  }

  createAddContForm() {
    this.addContactForm = this.fb.group({
      fName: [null, Validators.required],
      lName: [null, Validators.required],
      email: [null, Validators.required]
    });
  }

  viewRfq(rfqId) {
    this.router.navigate(['rfq/' + rfqId]);
  }
  trigRfqForm() {
    this.router.navigate(['new-rfq']);
  }
  handleChange(e) {
    console.log('switched');
  }

  promptRfqClose() {
    this.rfqID = this.selectedRfqObj.rfqSeq;
    this.prompt = 'You will not be able to receive quotes from vendors if this RFQ is closed';
    this.modalTitle = 'Close RFQ#: ' + this.rfqID;
    this.isDelRfq = true;
    this.isModal = true;
    // this.isAltContsClicked = false;
  }

  selRfq(rfqObj) {
    this.selectedRfqObj = rfqObj;
  }

  editRfq() {
    this.ngRedux.dispatch({ type: EDIT_RFQ, selected_rfq: this.selectedRfqObj });
    this.router.navigate(['rfq/' + this.selectedRfqObj.rfqSeq + '/edit']);
  }

  closeRfq() {
    const el = this.dom.find('#closeRfqTxtarea');
    if (!el) {
      return;
    }
    if (el.val().length === 0) {
      this.closeRfqErrMsg = true;
    } else {
      const obj = {
        rfqSeq: this.rfqID,
        custId: this.user._id,
        close_reason: this.dom.find('#closeRfqTxtarea').val()
      };
      this.rfqService.updateRfq(obj).subscribe(
        data => {
          if (data.success) {
            this.appComponent.showSuccessMsg(data.message);
            this.closeModal();
            this.getAllRfqs();
          } else {
            this.closeRfqErrMsg = true;
            this.rfqCancelPrompt = data.message;
          }
        },
        err => {
          this.closeRfqErrMsg = true;
          this.rfqCancelPrompt = 'Server Error: Please try again later';
        }
      );
      // this.rfqService.deleteRfq(obj).subscribe(
      //   data => {
      //     if (data.success) {
      //       this.appComponent.showSuccessMsg(data.message);
      //       this.closeModal();
      //       this.getAllRfqs();
      //     } else {
      //       this.closeRfqErrMsg = true;
      //       this.rfqCancelPrompt = data.message;
      //     }
      //   },
      //   err => {
      //     this.closeRfqErrMsg = true;
      //     this.rfqCancelPrompt = 'Server Error: Please try again later';
      //   }
      // );
    }
  }

  hideErrorTxt() {
    if (this.closeRfqErrMsg === true) {
      this.closeRfqErrMsg = false;
    }
  }

  closeModal() {
    this.addContactForm.reset();
    console.log('modal hidden');
    this.isDelRfq = false;
    this.hideErrorTxt();
    this.isModal = false;
    this.isAltContsClicked = false;
  }

  toggleAltContactForm() {
    this.retMsg = '';
    this.dom.find('#altContsDiv').slideToggle('slow');
  }

  showFoundUser(data) {
    this.isDifferentUserFound = true;
    this.diffUserFound = data;
  }

  // addAltContact(contact) {
  //   console.log(contact);
  //   // Checks if the alt contact to be added to the rfq, exists in our records and also is verified.
  //   this.appService.getCustomerByEmail(contact).subscribe(
  //     data => {
  //       // console.log(data);
  //       if (data.success) {
  //         if (data.cust.verified) {
  //           if (data.cust.fName !== contact.fName || data.cust.lName !== contact.lName) {
  //             // this.dom.find('#altContsDiv').slideUp('slow');
  //             this.diffUserFoundMsg = 'The first name and/or last name you entered seems incorrect . But we found the following user.';
  //             this.showFoundUser(data.cust);
  //           }
  //           console.log('cust is verified');
  //           // this.altcontacts.push(altContObj);
  //           // this.isAltContMsg = true;
  //           // this.errMsg = '';
  //           // this.successMsg = 'Contact Added';
  //           // this.emptyAltContField();
  //           // console.log(this.altcontacts);
  //         } else {
  //           // this.successMsg = '';
  //           console.log('cust NOT verified');
  //           // this.errMsg = 'This contact cannot be added yet on this rfq, as they have not been verified yet by ' + data.cust.compName;
  //           // this.isAltContMsg = true;
  //         }
  //         // this.errorMsg = ''
  //         this.addContactForm.reset();
  //       } else {
  //         console.log(data.message);
  //         // this.successMsg = '';
  //         this.errMsg = data.message;
  //         this.isAltContMsg = true;
  //       }

  //     },
  //     err => {
  //       // this.errMsg = 'Server Error: Please try again later';
  //       // this.isAltContMsg = true;
  //     }
  //   );
  // }

  cancelAltContact() {
    this.addContactForm.reset();
    this.dom.find('#altContsDiv').slideUp('slow');
  }
  cancelAddFoundUser() {
    this.isDifferentUserFound = false;
  }

  addFoundUser(user) {
    console.log(user);
  }

  addAltContact(cont) {
    console.log(cont);
    cont.pri_contact = this.user.email;
    cont.rfqSeq = this.rfqID;
    cont.isAdd = true;
    console.log(cont);
    this.rfqService.updateRfqAltContacts(cont).subscribe(
      data => {
        this.retMsg = data.message;
        if (data.success) {
          this.addContactForm.reset();
          this.dom.find('#msgDiv').removeClass('errorTxt');
          this.dom.find('#msgDiv').addClass('successTxt');
          this.getAllRfqs();
          this.selectedRfqAltContacts.push(data.cust);
        } else {
          this.dom.find('#msgDiv').removeClass('successTxt');
          this.dom.find('#msgDiv').addClass('errorTxt');
        }
      },
      err => {
        this.retMsg = 'Server Error. Please try again later';
        this.dom.find('#msgDiv').addClass('successTxt');
        this.dom.find('#msgDiv').addClass('errorTxt');
      }
    );
  }
  promptContDel(e, cont) {
    e.target.style.display = 'none';
    const el = document.getElementById(cont.email);
    el.style.display = 'block';
  }

  deleteAltContact(contact) {
    const cont = {
      email: contact.email,
      rfqSeq: this.rfqID,
      isAdd: false
    };
    this.rfqService.updateRfqAltContacts(cont).subscribe(
      data => {
        if (data.success) {
          console.log(data.message);
          const index = this.selectedRfqAltContacts.indexOf(contact);
          this.selectedRfqAltContacts = this.selectedRfqAltContacts.filter((val, i) => i != index);
          this.getAllRfqs();
        } else {
          console.log(data.message);
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  undoAltContDel(contEmail) {
    const parent = document.getElementById(contEmail);
    (<HTMLElement>parent.previousElementSibling).style.display = 'block';
    parent.style.display = 'none';
  }

  viewQuotes(rfqID) {
    console.log(rfqID);
    this.router.navigate(['rfq/' + rfqID + '/quotes']);
  }

}
