import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RfqService } from '../../services/rfq.service';

declare var $: any;

@Component({
  selector: 'app-rfq-quotes',
  templateUrl: './rfq-quotes.component.html',
  styleUrls: ['./rfq-quotes.component.css'],
  providers: [RfqService]
})
export class RfqQuotesComponent implements OnInit {
private isDataRet: boolean; private pgTitle: string; private rfqSeq: number;
private quotes; private emptyRfqMsg: string; private dom; private selectedOptionQuote;
private isNoQuotesCompsTrig: boolean; private noQuoteCompps; private selectedQuotes;
  constructor(private route: ActivatedRoute, private rfqService: RfqService,
    private elRef: ElementRef) { }

  ngOnInit() {
    this.selectedQuotes = [];
    this.isNoQuotesCompsTrig = false;
    this.dom = $(this.elRef.nativeElement);
    this.isDataRet = false;
    this.rfqSeq = Number(this.route.snapshot.params.id);
    this.getRfq(this.rfqSeq);
    this.noQuoteCompps = [
      'ABC Tech', 'Alliance Group', 'Suitland Inc'
    ];
  }

  getRfq(id) {
    const idParam = {rfqSeq: id};
    this.rfqService.getRfqById(idParam).subscribe(
      data => {
        console.log(data);
        this.isDataRet = true;
        this.pgTitle = 'RFQ #' + data.rfq.rfqSeq + ': ' + data.rfq.tag;
        this.quotes = data.rfq.quotes;
        setTimeout(() => {
          this.dom.find('.dropdown-trigger').dropdown();
        }, 10);
      },
      err => {
        this.isDataRet = true;
        console.log(err);
      }
    );
  }

  selRow(e) {
    console.log(e.data);
    console.log(this.selectedQuotes);
  }

  unSelRow(e) {
    console.log(e);
    console.log(this.selectedQuotes);
  }

  trigQuoteOptions(quote) {
    this.selectedOptionQuote = quote;
  }
  viewQuote() {
    console.log(this.selectedOptionQuote);
  }
  acceptQuote(quote) {
    console.log(this.selectedOptionQuote);
  }
  showCompsWithoutQuotes() {
    this.isNoQuotesCompsTrig = true;
  }
  dwnldQuote() {
    console.log(this.selectedOptionQuote);
  }
  dwnldAllQuotes() {
    console.log('downloading all quotes');
  }
}
