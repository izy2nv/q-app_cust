import { Component, OnInit} from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public url = 'http://localhost:3000/api/rfq_file_upload';
  public companyId; public name: string;
  public user;
  constructor() {
  }
  ngOnInit() {
    // this.getLoggedInUser();
  }

  // getLoggedInUser() {
  //   this.user = JSON.parse(localStorage.getItem('user'));
  //   if (this.user !== null) {
  //     this.name = this.user.fName + ' ' + this.user.lName;
  //   } else {
  //     return;
  //   }
  // }

  showSuccessMsg(msg) {
    // M.toast({html: msg, displayLength: 2000, classes: 'success'});
  }

  showErrorMsg(msg) {
    // M.toast(msg, 2000, 'error');
  }

  formatPhoneNo(no) {
    const s2 = ('' + no).replace(/\D/g, '');
    const m = s2.match(/^(\d{3})(\d{3})(\d{4})$/);
    return (!m) ? null : '(' + m[1] + ')' + m[2] + '-' + m[3];
  }
}
