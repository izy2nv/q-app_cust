export const USER_LOGGEDIN = 'USER_LOGGEDIN';
export const FETCH_ALL_RFQS = 'FETCH_ALL_RFQS';
export const SET_ALL_RFQS = 'SET_ALL_RFQS';
export const EDIT_RFQ = 'EDIT_RFQ';
export const ARCHIVE_RFQ = 'ARCHIVE_RFQ';
export const UNARCHIVE_RFQ = 'UNARCHIVE_RFQ';
export const SAVE_RFQ = 'SAVE_RFQ';
export const DELETE_RFQ = 'DELETE_RFQ';
export const RESTORE_RFQ = 'RESTORE_RFQ';
export const DELETE_ALL_RFQS = 'DELETE_ALL_RFQS';
export const DELETE_ALT_CONT = 'DELETE_ALT_CONT';
