import { Routes } from '@angular/router';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { WhitepaperComponent } from './components/whitepaper/whitepaper.component';
import { VideosComponent } from './components/videos/videos.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AuthGuard } from './guards/auth.guard';
import { PwResetComponent } from './components/pw-reset/pw-reset.component';
import { CompaniesComponent } from './components/companies/companies.component';
import { CompanyComponent } from './components/company/company.component';
import { CustVerificationComponent } from './components/cust-verification/cust-verification.component';
import { QuotesComponent } from './components/quotes/quotes.component';
import { RfqComponent } from './components/rfq/rfq.component';
import { NewRfqComponent } from './components/new-rfq/new-rfq.component';
import { RfqEditComponent } from './components/rfq-edit/rfq-edit.component';
import { RfqQuotesComponent } from './components/rfq-quotes/rfq-quotes.component';
import { NewCompanyComponent } from './components/new-company/new-company.component';
export const appRoutes: Routes = [
    { path: 'company_register', component: NewCompanyComponent },
    { path: '', component: LandingPageComponent },
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
    { path: 'videos', component: VideosComponent },
    { path: 'white-paper', component: WhitepaperComponent },
    { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard], runGuardsAndResolvers: 'always'},
    { path: 'pw-reset/:id', component: PwResetComponent },
    { path: 'companies', component: CompaniesComponent },
    { path: 'companyDetails', component: CompanyComponent },
    { path: 'cust-verify/:id', component: CustVerificationComponent },
    { path: 'quotes', component: QuotesComponent },
    { path: 'rfq/:id', component: RfqComponent },
    { path: 'rfq/:id/edit', component: RfqEditComponent },
    { path: 'new-rfq', component: NewRfqComponent },
    { path: 'rfq/:id/quotes', component: RfqQuotesComponent }
];
