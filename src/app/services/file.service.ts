import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs';


@Injectable()
export class FileService {

  // rfqDownload
  private rfqDownload = 'api/download';

  constructor(private http: HttpClient) { }

  downloadFile (fileName) {
    const body = {fileName: fileName};
    return this.http.post(this.rfqDownload, body, {
      responseType: 'blob',
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    });
}

}
