import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
  private userAuthUrl = 'customers/authenticate';
  private userProfileUrl = 'customers/profile';
  private fpWresetLinkRequestUrl = 'customers/forgotpw';
  private resetPwUrl = 'customers/resetpw';
  private retrieveUnUrl = 'customers/retrieve-un';

  authToken: any;
  user: any;

  constructor(private http: Http) { }

  authenticateUser(obj) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.userAuthUrl, obj, { headers: headers }).map(data => data.json());
  }

  storeUserData(token, user) {
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));  // localstorage only stores strings, not objects
    this.authToken = token;
    this.user = user;
  }

  // Logs user out by removing user token from localstorage
  removeUserData() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

  retrieveToken() {
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  getUserProfile() {
    this.retrieveToken();
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authToken); // Needed cos /profile is being protected, and needs token for authorization
    return this.http.get(this.userProfileUrl, { headers: headers }).map(data => data.json());
  }

  // Checks if user is still logged in and their token hasn't expired.
  // Its used to control the login, logout and other btns we don't want displayed if user is not logged in
  loggedIn() {
    return tokenNotExpired('id_token');
  }

  // checks if logged-in customer is verified by company
  verifyCust() {
    const loggedInUser = JSON.parse(localStorage.getItem('user'));
    if (loggedInUser) {
      return loggedInUser.verified;
    } else {
      return false;
    }
  }

  // Send user a pw reset link in their email
  sendPwResetLinkToUser(user) {
    console.log(user);
    // const headers = new Headers();
    // headers.append('Content-Type', 'application/json');
    return this.http.post(this.fpWresetLinkRequestUrl, user).map(data => data.json());
  }

  // Reset user's pw using forgot pw token sent to their email
  resetPw(resetObj) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.resetPwUrl, resetObj, {headers: headers}).map(data => data.json());
  }

  retriveUn(user) {
    return this.http.post(this.retrieveUnUrl, user).map(data => data.json());
  }
}
