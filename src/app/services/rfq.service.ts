import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { NgRedux } from '@angular-redux/store';

@Injectable()
export class RfqService {
  // Get all prod/services categories
  private catsUrl = '/rfqs/cats';
  // Get rfq by rfqID
  private oneRfqUrl = '/rfqs/rfq';
  // Get rfq details by entered param
  private allCustRfqUrl = '/rfqs/cust_rfqs';
  // create new rfq
  private newRfqUrl = '/rfqs/new_rfq';
  // Delete an Rfq
  private deleteOneRfqUrl = '/rfqs/deleteOne';
  // Update Rfq
  private update_rfq = '/rfqs/update_rfq';

  private addAltContToRfqUrl = '/rfqs/addContact';
  private delAltContFrmRfqUrl = '/rfqs/delete_altcontact';

  constructor(private http: Http, private ngRedux: NgRedux<any>) { }

  getAllProdServCats() {
    return this.http.get(this.catsUrl).map(data => data.json());
  }

  getRfqById(id) {
    return this.http.post(this.oneRfqUrl, id).map(data => data.json());
  }

  getAllRfqsBySearchParam(param) {
    console.log(param);
    return this.http.post(this.allCustRfqUrl, param).map(data => data.json());
  }

  createNewRfq(rfqObj) {
    return this.http.post(this.newRfqUrl, rfqObj).map(data => data.json());
  }

  deleteRfq(obj) {
    console.log(obj);
    return this.http.post(this.deleteOneRfqUrl, obj).map(data => data.json());
  }

  updateRfqAltContacts(cont) {
    console.log(cont);
    if (cont.isAdd) {
      console.log('contact is being added');
      return this.http.post(this.addAltContToRfqUrl, cont).map(data => data.json());
    } else {
      console.log('contact is being deleted');
      return this.http.post(this.delAltContFrmRfqUrl, cont).map(data => data.json());
    }
  }

  // Update an Rfq
  updateRfq(rfqObj) {
    return this.http.post(this.update_rfq, rfqObj).map(data => data.json());
  }
}
