import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class AppService {


  private footerLinksUrl = '/api/footerlinks';
  // Products end-points
  private allProdsUrl = 'api/allprods';

  // Customer end-points
  private signupUrl = 'customers/register';
  private updateProfileUrl = 'customers/updateprofile';
  private verifyCustUrl = 'customers/verify_cust';
  private getCustByEmailUrl = 'customers/getby_email';

  // CH-Companies end-points
  private chcompaniesUrl = 'api/ch_companies';
  private chcompanyUrl = 'api/ch';

  // Cust-Companies end-points
  private custCompaniesUrl = 'customers/cust_companies';
  private saveUnverifiedCustCompUrl = 'customers/saveUnverifiedCustComp';

  // Ministries
  private ministriesUrl = 'ministries/ministries';

  // Countries
  private statesUrl = 'api/states';

  constructor(private http: Http) { }

  getFooterLinks(): Observable<any> {
    return this.http.get(this.footerLinksUrl).map(links => links.json());
  }

  getAllProducts(): Observable<any> {
    return this.http.get(this.allProdsUrl).map(data => data.json());
  }

  // Customers end-point implementations
  registerNewCust(cust) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.signupUrl, cust, {headers: headers}).map(newCust => newCust.json());
  }
  updateCustProfile(updatedProfile) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.updateProfileUrl, updatedProfile, {headers: headers}).map(data => data.json());
  }
  getAllVerifiedCustCompanies(flag) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.custCompaniesUrl, flag).map(data => data.json());
  }

  // getAllVerifiedMinistries(flag) {
  //   const headers = new Headers();
  //   headers.append('Content-Type', 'application/json');
  //   return this.http.post(this.ministriesUrl, flag).map(data => data.json());
  // }
  getMinistries(param) {
    console.log(param);
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.ministriesUrl, param).map(data => data.json());
  }

  saveUnverifiedCustComp(comp) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.saveUnverifiedCustCompUrl, comp, {headers: headers}).map(newCust => newCust.json());
  }

  // verify customer
  verifyCustomer(id) {
    const custId = {id: id};
    return this.http.post(this.verifyCustUrl, custId).map(data => data.json());
  }

  // CH end-points implementations
  getAllCompanies(): Observable<any> {
    return this.http.get(this.chcompaniesUrl).map(data => data.json());
  }
  getCHcompanyById(id) {
    const query = {id: id};
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.chcompanyUrl, query).map(data => data.json());
  }
  getCustomerByEmail(cust) {
    return this.http.post(this.getCustByEmailUrl, cust).map(data => data.json());
  }
  getStatesByCountry(param) {
    return this.http.post(this.statesUrl, param).map(data => data.json());
  }
}
