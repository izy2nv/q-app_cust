export interface IRfq {
        _id: number;
        tag: string;
        rfqType: string;
        cat: string;
        subCat: string;
        other: string;
        desc: string;
        bidEndDate: any;
        attachmentList: Array<any>;
        alt_contacts: Array<any>;
        pri_contact: Object;
        status: number;
        rfqSeq: number;
}
