import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

// Primeng modules
import { TableModule } from 'primeng/table';
import { ButtonModule, MessagesModule, InputSwitchModule, DropdownModule,
         DialogModule, CalendarModule, SidebarModule } from 'primeng/primeng';
import {TabViewModule} from 'primeng/tabview';

import { FileUploadModule } from 'ng2-file-upload';
import { ChartsModule } from 'ng2-charts';

// Routes Guard
import { AuthGuard } from './guards/auth.guard';

// Redux
import { NgRedux, NgReduxModule } from '@angular-redux/store';
import { IAppState, rootReducer, INITIAL_STATE } from './store';

import { AppComponent } from './app.component';
import { AppService } from './services/app.service';
import { RfqService } from './services/rfq.service';
import { AuthService } from './services/app.authService';
import { appRoutes } from './app.routes';
import { LoginComponent } from './components/login/login.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { WhitepaperComponent } from './components/whitepaper/whitepaper.component';
import { VideosComponent } from './components/videos/videos.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { SignupComponent } from './components/signup/signup.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';
import { PwResetComponent } from './components/pw-reset/pw-reset.component';
import { CompaniesComponent } from './components/companies/companies.component';
import { CompanyComponent } from './components/company/company.component';
import { CustVerificationComponent } from './components/cust-verification/cust-verification.component';
import { QuotesComponent } from './components/quotes/quotes.component';
import { RfqComponent } from './components/rfq/rfq.component';
import { NewRfqComponent } from './components/new-rfq/new-rfq.component';
import { RfqEditComponent } from './components/rfq-edit/rfq-edit.component';
import { from } from 'rxjs/observable/from';
import { RfqQuotesComponent } from './components/rfq-quotes/rfq-quotes.component';
import { NewCompanyComponent } from './components/new-company/new-company.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    WhitepaperComponent,
    VideosComponent,
    LandingPageComponent,
    SignupComponent,
    DashboardComponent,
    ProfileComponent,
    PwResetComponent,
    CompaniesComponent,
    CompanyComponent,
    CustVerificationComponent,
    QuotesComponent,
    RfqComponent,
    NewRfqComponent,
    RfqEditComponent,
    RfqQuotesComponent,
    NewCompanyComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes, {onSameUrlNavigation: 'reload'}),
    TableModule,
    MessagesModule,
    InputSwitchModule,
    FileUploadModule,
    ButtonModule,
    CalendarModule,
    DialogModule,
    DropdownModule,
    SidebarModule,
    TabViewModule,
    ChartsModule,
    NgReduxModule
  ],
  providers: [AppService, AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(ngRedux: NgRedux<IAppState>) {
    ngRedux.configureStore(rootReducer, INITIAL_STATE);
  }
}
