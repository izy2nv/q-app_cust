import { IRfq } from './models/rfq.model';
import { EDIT_RFQ, ARCHIVE_RFQ, UNARCHIVE_RFQ, SET_ALL_RFQS, DELETE_RFQ,
  RESTORE_RFQ, DELETE_ALT_CONT } from './actions';

export interface IAppState {
  allRfqs;
  archived_rfqs;
  unArchived_rfqs;
  deleted_rfqs;
  selected_rfq;
}

export const INITIAL_STATE: IAppState = {
  allRfqs: [],
  archived_rfqs: [],
  unArchived_rfqs: [],
  deleted_rfqs: [],
  selected_rfq: JSON.parse(localStorage.getItem('rfq_edit'))
};

export function rootReducer(state, action) {
  switch (action.type) {
    case SET_ALL_RFQS:
    const unArchived_rfqs = [];
    const archived_rfqs = [];
    const deletedRfqs = [];
    // rfq['state'] here indicates if an rfq has bn archived, unarchived or deleted
    for (const rfq of action.allRfqs) {
      if (rfq['state'] === 1) {
        unArchived_rfqs.push(rfq);
      } if (rfq['state'] === 2) {
        archived_rfqs.push(rfq);
      } else if (rfq['state'] === 3) {
        deletedRfqs.push(rfq);
      }
    }
    return Object.assign({}, state, {
      allRfqs: action.allRfqs,
      archived_rfqs: archived_rfqs,
      unArchived_rfqs: unArchived_rfqs,
      deleted_rfqs: deletedRfqs
    });

    case ARCHIVE_RFQ:
    return Object.assign({}, state, {
      archived_rfqs: state.archived_rfqs.concat(Object.assign({}, action.archived_rfq)),
      unArchived_rfqs: state.unArchived_rfqs.filter(rfq => rfq._id !== action.archived_rfq._id)
    });

    case UNARCHIVE_RFQ:
    return Object.assign({}, state, {
      unArchived_rfqs: state.unArchived_rfqs.concat(Object.assign({}, action.unArchived_rfq)),
      archived_rfqs: state.archived_rfqs.filter(rfq => rfq._id !== action.unArchived_rfq._id)
    });

    case EDIT_RFQ:
      localStorage.setItem('rfq_edit', JSON.stringify(action.selected_rfq));
      return Object.assign({}, state, {
        selected_rfq: action.selected_rfq
      });

      case DELETE_RFQ:
    return Object.assign({}, state, {
      deleted_rfqs: state.deleted_rfqs.concat(Object.assign({}, action.deleted_rfq)),
      unArchived_rfqs: state.unArchived_rfqs.filter(rfq => rfq._id !== action.deleted_rfq._id),
      archived_rfqs: state.archived_rfqs.filter(rfq => rfq._id !== action.deleted_rfq._id)
    });

    case RESTORE_RFQ:
    return Object.assign({}, state, {
      deleted_rfqs: state.deleted_rfqs.filter(rfq => rfq._id !== action.restored_rfq._id),
      unArchived_rfqs: state.unArchived_rfqs.concat(Object.assign({}, action.restored_rfq))
    });

    case DELETE_ALT_CONT:
      state.selected_rfq.alt_contacts = state.selected_rfq.alt_contacts.filter(cont => cont.email !== action.payload);
      localStorage.setItem('rfq_edit', JSON.stringify(state.selected_rfq));
      return Object.assign({}, state, {
        selected_rfq: state.selected_rfq
      });

    // default:
  }
  return state;
}
