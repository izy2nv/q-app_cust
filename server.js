const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const app = express();
const passport = require('passport');
const mongoose = require('mongoose');
const cors = require('cors');
const config = require('./config/db');

// mongoose.connect(config.database);

// Tells us if server is connected to the DB
//  mongoose.connection.on('connected', () => {
//      console.log('Connected to Database: ' + config.database);
//  });

 // Handles error if server can't connect to DB
//  mongoose.connection.on('error', (err) => {
//     console.log('ERROR: Server could not connect to Database' + '\n' + err);
// });

//Set port
const port = process.env.PORT || '3000';
app.use(cors());
app.listen(port, () => {
    console.log('Server started on port: '+ port);
});

const api = require('./routes/api');
const customers = require('./routes/customers');
const rfqs = require('./routes/rfqs');
const chs = require('./routes/chs');
const ministries = require('./routes/ministries');

//Set the body parser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

//Set Passport middleware for authenticating user
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);

//Point server to Angular DIST output folder
app.use(express.static(path.join(__dirname, 'dist')));

//Set API file location
app.use('/api', api);
app.use('/customers', customers);
app.use('/rfqs', rfqs);
app.use('/chs', chs);
app.use('/ministries', ministries);

//Forward all other requests to the Angular index.html file
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});