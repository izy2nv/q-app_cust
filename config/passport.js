const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const Customer = require('../models/customer');
const Ministry = require('../models/ministry');
const dbConfig = require('../config/db');

module.exports = function (passport) {
  let opts = {},
    error, authUser;
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
  opts.secretOrKey = dbConfig.secret;
  passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
    Customer.getCustomerById(jwt_payload._id, (err, cust) => {
      if (err) {
        return done(err, false);
      }
      if (cust) {
        if (cust.min) {
          var dbRef = cust.min;
          Ministry.getMinistryDetails(dbRef, (err, ministry) => {
            if (err) throw err;
            delete ministry._id;
            cust.min = ministry;
            return done(null, cust);
          });
        } else {
          return done(null, cust);
        }
      } else {
        return done(null, false);
      }
    });
  }));
};